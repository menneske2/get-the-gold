import Levels.LevelShop;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik
 */
public class CharacterSelect extends Menu{
	
	public static int sbgID = 3;
	private final List<Class> choices;
	private int nextLevel = 5;
	
	int page = 0;
	private Class selectedCharacter;
	private int selectedTile = 0;
	
	
	private String img1Folder, img2Folder, img3Folder, img4Folder;
	
	private Image backgroundImg;
	private Image playImg, backImg, arrowNextImg, arrowPrevImg;
	private Image playImgActive, backImgActive, arrowNextImgActive, arrowPrevImgActive;
	private final String titleText = "Select a character";
	
	private String char1text, char2text, char3text, char4text;
	private Image char1Img, char2Img, char3Img, char4Img;
	// The positions of the different buttons on the main menu. coords starts from top-left corner
	private int titlePosX, titlePosY;
    private int playPosX, playPosY;
	private int backPosX, backPosY;
	private int arrowPrevX, arrowPrevY, arrowNextX, arrowNextY;
	private int char1X, char1Y, char2X, char2Y, char3X, char3Y, char4X, char4Y;
	private int char1textX, char1textY, char2textX, char2textY, char3textX, char3textY, char4textX, char4textY;
	
	private boolean playActive, backActive, arrowPrevActive, arrowNextActive;
	
	// These are mostly used in init(), but are needed for render() to correct people that use the wrong resolution.
	int imgWidth = 64;
	int imgHeight = 128;
	
	
	public CharacterSelect(List<Class> choices){
		this.choices = choices;
	}
	
	@Override
	public int getID() {
		return this.sbgID;
	}
	
	public void setNextLevel(int nextLevel){
		this.nextLevel = nextLevel;
	}
	
	
	/**
	 * Initializes the positions for various GUI elements based on screen size.
	 * @param gc
	 * @param sbg
	 * @throws SlickException 
	 */
    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
    {
        super.init(gc, sbg);
		
		// Update the character images, names etc.
		updateChoices();
		
		// Constants for the GUI layout.
		int imgSpacingX = 300;
		int imgSpacingY = 100;
		int squaresStartY = 15; // The Y position from which we start drawing the squares. Given in percentage.
		int imgTextSpacing = 15; // The space between a level thumbnail and the level name text.
		
		
		backgroundImg = new Image("resource/GUI/background.jpg");
		arrowNextImg = new Image("resource/GUI/arrowRight.png");
		arrowPrevImg = new Image("resource/GUI/arrowLeft.png");
		arrowNextImgActive = new Image("resource/GUI/arrowRightActive.png");
		arrowPrevImgActive = new Image("resource/GUI/arrowLeftActive.png");
		playImg = new Image("resource/GUI/buttonPlay.png");
		playImgActive = new Image("resource/GUI/buttonPlayActive.png");
		backImg = new Image("resource/GUI/buttonBack.png");
		backImgActive = new Image("resource/GUI/buttonBackActive.png");
		
		// Set the positions for UI elements based on screen size and the constants above.
		titlePosX = (gc.getWidth()-font.getWidth(titleText))/2;
		titlePosY = gc.getHeight()/100*5;
		playPosX = (gc.getWidth()-playImg.getWidth())/2;
		playPosY = gc.getHeight()/10*8;
		backPosX = (gc.getWidth()-backImg.getWidth())/2;
		backPosY = gc.getHeight()/10*9;
		arrowNextX = gc.getWidth()/10 * 8;
		arrowPrevX = gc.getWidth()/10 * 2 - arrowPrevImg.getWidth();
		arrowNextY = gc.getHeight()/100*squaresStartY + imgHeight+imgSpacingY/2 - arrowNextImg.getHeight()/2;
		arrowPrevY = gc.getHeight()/100*squaresStartY + imgHeight+imgSpacingY/2 - arrowPrevImg.getHeight()/2;
		char1X = gc.getWidth()/2 - (imgWidth+imgSpacingX/2);
		char1Y = gc.getHeight()/100*squaresStartY;
		char2X = gc.getWidth()/2 + imgSpacingX/2;
		char2Y = gc.getHeight()/100*squaresStartY;
		char3X = gc.getWidth()/2 - (imgWidth+imgSpacingX/2);
		char3Y = gc.getHeight()/100*squaresStartY + (imgHeight+imgSpacingY);
		char4X = gc.getWidth()/2 + imgSpacingX/2;
		char4Y = gc.getHeight()/100*squaresStartY + (imgHeight+imgSpacingY);
		
		char1textX = gc.getWidth()/2 - (imgSpacingX+imgWidth)/2 - font.getWidth(char1text)/2;
		char2textX = gc.getWidth()/2 + (imgSpacingX+imgWidth)/2 - font.getWidth(char2text)/2;
		char3textX = gc.getWidth()/2 - (imgSpacingX+imgWidth)/2 - font.getWidth(char3text)/2;
		char4textX = gc.getWidth()/2 + (imgSpacingX+imgWidth)/2 - font.getWidth(char4text)/2;
		char1textY = gc.getHeight()/100*squaresStartY + imgHeight + imgTextSpacing;
		char2textY = gc.getHeight()/100*squaresStartY + imgHeight + imgTextSpacing;
		char3textY = gc.getHeight()/100*squaresStartY + 2*imgHeight + imgSpacingY + imgTextSpacing;
		char4textY = gc.getHeight()/100*squaresStartY + 2*imgHeight + imgSpacingY + imgTextSpacing;
		
    }
	
	@Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
    {
		backgroundImg.draw(0, 0, gc.getWidth(), gc.getHeight());
		
		// Yellow square around selected level.
		int offset = 15;
		Rectangle selectBox = null;
		switch(selectedTile){
			case 0:
				selectBox = new Rectangle(char1X - offset, char1Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 1:
				selectBox = new Rectangle(char2X - offset, char2Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 2:
				selectBox = new Rectangle(char3X - offset, char3Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 3:
				selectBox = new Rectangle(char4X - offset, char4Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
		}
		
		
		g.setColor(new Color(200, 200, 0, 150)); // r, g, b, a
		g.fill(selectBox);
		
		
		if(playActive)	playImgActive.draw(playPosX, playPosY);
		else			playImg.draw(playPosX, playPosY);
		if(backActive)	backImgActive.draw(backPosX, backPosY);
		else			backImg.draw(backPosX, backPosY);
		if(arrowPrevActive)	arrowPrevImgActive.draw(arrowPrevX, arrowPrevY);
		else				arrowPrevImg.draw(arrowPrevX, arrowPrevY);
		if(arrowNextActive)	arrowNextImgActive.draw(arrowNextX, arrowNextY);
		else				arrowNextImg.draw(arrowNextX, arrowNextY);
		
		g.setColor(Color.white);
        g.setFont(font);
		
		g.drawString(char1text, char1textX, char1textY);
		g.drawString(char2text, char2textX, char2textY);
		g.drawString(char3text, char3textX, char3textY);
		g.drawString(char4text, char4textX, char4textY);
		g.drawString(titleText, titlePosX, titlePosY);
		if(char1Img != null) char1Img.draw(char1X, char1Y, imgWidth, imgHeight);
		if(char2Img != null) char2Img.draw(char2X, char2Y, imgWidth, imgHeight);
		if(char3Img != null) char3Img.draw(char3X, char3Y, imgWidth, imgHeight);
		if(char4Img != null) char4Img.draw(char4X, char4Y, imgWidth, imgHeight);
		
    }
	
	
	/**
	 * Updates the character previews on screen. Call after changing which pictures should be in the center (which you do by changing the page variable).
	 */
	private void updateChoices(){
		
		// Advanced bullshittery. Getting the value of a static field from a class object.
		try{ img1Folder = (String) choices.get(4*page).getDeclaredField("FOLDER").get(null); } catch(Exception e){ img1Folder = null; }
		try{ img2Folder = (String) choices.get(4*page+1).getDeclaredField("FOLDER").get(null); } catch(Exception e){ img2Folder = null; }
		try{ img3Folder = (String) choices.get(4*page+2).getDeclaredField("FOLDER").get(null); } catch(Exception e){ img3Folder = null; }
		try{ img4Folder = (String) choices.get(4*page+3).getDeclaredField("FOLDER").get(null); } catch(Exception e){ img4Folder = null; }
		
		Path path1;
		Path path2;
		Path path3;
		Path path4;
		
		try { path1 = Paths.get(img1Folder+"/name.txt"); char1text = Files.readAllLines(path1).get(0); } catch(Exception e){ char1text = ""; }
		try { path2 = Paths.get(img2Folder+"/name.txt"); char2text = Files.readAllLines(path2).get(0); } catch(Exception e){ char2text = ""; }
		try { path3 = Paths.get(img3Folder+"/name.txt"); char3text = Files.readAllLines(path3).get(0); } catch(Exception e){ char3text = ""; }
		try { path4 = Paths.get(img4Folder+"/name.txt"); char4text = Files.readAllLines(path4).get(0); } catch(Exception e){ char4text = ""; }
		
		// Thumbnails are scaled to imgWidth x imgHeight. you can find those just before the constructor.
		try{ char1Img = new Image(img1Folder+"/thumbnail.png"); } catch(Exception e){ char1Img = null; }
		try{ char2Img = new Image(img2Folder+"/thumbnail.png"); } catch(Exception e){ char2Img = null; }
		try{ char3Img = new Image(img3Folder+"/thumbnail.png"); } catch(Exception e){ char3Img = null; }
		try{ char4Img = new Image(img4Folder+"/thumbnail.png"); } catch(Exception e){ char4Img = null; }
			
	}
	

	@Override
	protected void detectMouse(int button, int x, int y) {
		if(sbg.getCurrentStateID()==sbgID){
			if(isHoveringOverImage(playImg, playPosX, playPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonPlay();
				}
			}
			if(isHoveringOverImage(backImg, backPosX, backPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonBack();
				}
			}
			if(isHoveringOverImage(arrowNextImg, arrowNextX, arrowNextY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonArrowNext();
				}
			}
			if(isHoveringOverImage(arrowPrevImg, arrowPrevX, arrowPrevY, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonArrowPrev();
				}
			}
			if(isHoveringOverRectangle(char1X+imgWidth, char1Y+imgHeight, char1X, char1Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=0;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(char2X+imgWidth, char2Y+imgHeight, char2X, char2Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=1;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(char3X+imgWidth, char3Y+imgHeight, char3X, char3Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=2;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(char4X+imgWidth, char4Y+imgHeight, char4X, char4Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=3;
					select(prevTile);
				}
			}
		}
	}

	@Override
	protected void detectKey(int key, char c) {
		if(sbg.getCurrentStateID()==sbgID){
			if(key==Input.KEY_SPACE || key==Input.KEY_ENTER){
				buttonPlay();
			}
			if(key==Input.KEY_D || key==Input.KEY_RIGHT){
				selectRight();
			}
			if(key==Input.KEY_A || key==Input.KEY_LEFT){
				selectLeft();
			}
			if(key==Input.KEY_W || key==Input.KEY_UP){
				selectUp();
			}
			if(key==Input.KEY_S || key==Input.KEY_DOWN){
				selectDown();
			}
			if(key==Input.KEY_ESCAPE){
				buttonBack();
			}
		}
	}

	@Override
	protected void detectMouseMove(int oldX, int oldY, int newX, int newY) {
		playActive = isHoveringOverImage(playImg, playPosX, playPosY, newX, newY);
		backActive = isHoveringOverImage(backImg, backPosX, backPosY, newX, newY);
		arrowPrevActive = isHoveringOverImage(arrowPrevImg, arrowPrevX, arrowPrevY, newX, newY);
		arrowNextActive = isHoveringOverImage(arrowNextImg, arrowNextX, arrowNextY, newX, newY);
	}

	
	/**
	 * Takes the player to the shop, and sets the exit of the shop to the chosen level.
	 */
	private void buttonPlay(){
		LevelShop shop = (LevelShop) sbg.getState(LevelShop.sbgID);
		shop.setNextLevel(nextLevel);
		shop.setCharacter(selectedCharacter);
		try {
			shop.init(gc, sbg);
		} catch (SlickException ex){}
		sbg.enterState(LevelShop.sbgID);
	}
	
	/**
	 * Takes you back to the main menu.
	 */
	private void buttonBack(){
		sbg.enterState(WorldMap.sbgID);
	}
	
	/**
	 * Scrolls the levels on screen to the left.
	 */
	private void buttonArrowNext(){
		page++;
		if(page >= Math.ceil(choices.size() / 4f))
			page=0;
		updateChoices();
		while(!select(selectedTile)){
			selectedTile--;
		}
	}
	
	/**
	 * Scrolls the levels on screen to the right.
	 */
	private void buttonArrowPrev(){
		page--;
		if(page < 0 && Math.floor(choices.size() / 4f) == choices.size() / 4f)
			page = choices.size() / 4 - 1 ;
		else if(page < 0)
			page = (int) Math.floor(choices.size() / 4f);
		updateChoices();
		while(!select(selectedTile)){
			selectedTile--;
		}
	}
	
	/**
	 * Updates the "selectedLevel" variable to the level that is currently highlighted.
	 */
	private boolean select(int prevTile){
		Class character;
		try{ character = choices.get(4*page + selectedTile);
		} catch(Exception e){ character = null; }
		if(character != null){
			selectedCharacter = character;
			return true;
		}
		else{
			selectedTile = prevTile;
			return false;
		}
	}
	
	/**
	 * Moves the highlighting reticle one to the right. If it's already at the edge, it calls buttonArrowNext() and sets the highlight to the left.
	 */
	private void selectRight(){
		int prevTile = selectedTile;
		if(selectedTile == 1 || selectedTile == 3){
			selectedTile--;
			buttonArrowNext();
		}
		else{
			selectedTile++;
		}
		select(prevTile);
	}
	
	/**
	 * Moves the highlighting reticle one to the left. If it's already at the edge, it calls buttonArrowPrev() and sets the highlight to the right.
	 */
	private void selectLeft(){
		int prevTile = selectedTile;
		if(selectedTile == 0 || selectedTile == 2){
			selectedTile++;
			buttonArrowPrev();
		}
		else{
			selectedTile--;
		}
		select(prevTile);
	}
	
	/**
	 * Moves the highlighting reticle up. No wraparound.
	 */
	private void selectUp(){
		int prevTile = selectedTile;
		if(selectedTile == 2 || selectedTile == 3){
			selectedTile -= 2;
			select(prevTile);
		}
	}
	
	/**
	 * Moves the highlighting reticle down. No wraparound.
	 */
	private void selectDown(){
		int prevTile = selectedTile;
		if(selectedTile == 0 || selectedTile == 1){
			selectedTile += 2;
			select(prevTile);
		}
	}
	
}
