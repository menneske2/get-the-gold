
import java.awt.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Fredrik
 */
public abstract class Menu extends BasicGameState{
	
	// Variables needed for other menus.
	protected StateBasedGame sbg;
	protected GameContainer gc;
	protected UnicodeFont font;
	private boolean acceptingInput = false; // Used in the worst fix ever. Making sure input is only called once.
	
	protected Menu(){
		
	}
	

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		
		this.sbg = sbg;
		this.gc = gc;
		Input input = gc.getInput();
		
		// Setting up the font. It has to be done in init().
		font = new UnicodeFont(new java.awt.Font("Arial", java.awt.Font.PLAIN, 32));
		font.addAsciiGlyphs();
		font.getEffects().add(new ColorEffect(java.awt.Color.white));
		font.getEffects().add(new OutlineEffect(1, Color.BLACK));
		font.loadGlyphs();
		
		//Setting up listeners for keyboard and mouse input. 
		//Override protected void detectMouse(int button, int x, int y, int clickCount) to detect mouse inputs in subclasses.
		//Override protected void detectKey(int key, char c) to detect keyboard inputs  in  subclasses. 
		MouseListener ml = new MouseListener(){
			
			@Override
			public void mouseClicked(int button, int x, int y, int clickCount) {}
			@Override
			public void mousePressed(int button, int x, int y) {
				detectMouse(button, x, y);
			}
			@Override
			public void mouseReleased(int button, int x, int y) {	}
			@Override
			public void mouseWheelMoved(int change) {}
			@Override
			public void mouseMoved(int oldx, int oldy, int newx, int newy) {
				detectMouseMove(oldx, oldy, newx, newy);
			}
			@Override
			public void mouseDragged(int oldx, int oldy, int newx, int newy) {}
			@Override
			public void setInput(Input input) {}
			@Override
			public boolean isAcceptingInput() {
				acceptingInput = !acceptingInput;
				return acceptingInput;
			}
			@Override
			public void inputEnded() {}
			@Override
			public void inputStarted() {}
			
		};
		KeyListener kl = new KeyListener(){
			
			@Override
			public void keyReleased(int key, char c) {}
			@Override
			public void keyPressed(int key, char c) {
				detectKey(key, c);
			}
			@Override
			public void setInput(Input input) {}
			@Override
			public boolean isAcceptingInput() {
				acceptingInput = !acceptingInput;
				return acceptingInput;
			}
			@Override
			public void inputEnded() {}
			@Override
			public void inputStarted() {}
		};
		input.addKeyListener(kl);
		input.addMouseListener(ml);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics grphcs) throws SlickException {
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int i) throws SlickException {
		
	}
	
	/**
	 * Checks if the mouse is currently hovering over a drawn string.
	 * @param text The contents of the string.
	 * @param x The X coordinate of the string. (leftmost side)
	 * @param y The Y coordinate of the string. (topmost side)
	 * @param mouseX The X coordinate of the mouse.
	 * @param mouseY The Y coordinate of the mouse.
	 * @return true if the mouse is over the area. false otherwise.
	 */
	protected boolean isHoveringOverString(String text, int x, int y, int mouseX, int mouseY){
		return (mouseX>x && mouseX<x+font.getWidth(text)) && (mouseY>y && mouseY<y+font.getHeight(text));
	}
	

	/**
	 * Checks if the mouse is currently hovering over a drawn image.
	 * @param img The image you wish to check.
	 * @param imgX X coordinate of the image.
	 * @param imgY Y coordinate of the image.
	 * @param mouseX X coordinate of the mouse.
	 * @param mouseY Y coordinate of the mouse.
	 * @return true if the mouse is over the area. false otherwise.
	 */
	protected boolean isHoveringOverImage(Image img, int imgX, int imgY, int mouseX, int mouseY){
		if(img == null)
			return false;
		return (mouseX>imgX && mouseX<(imgX+img.getWidth())) && (mouseY>imgY && mouseY<(imgY+img.getHeight()));
	}
	
	// I have width and height before X and Y to make it easier to convert between the different "isHoveringOver" functions.
	protected boolean isHoveringOverRectangle(int width, int height, int imgX, int imgY, int mouseX, int mouseY){
		return (mouseX>imgX && mouseX<(imgX+width)) && (mouseY>imgY && mouseY<(imgY+height));
	}
	
	/**
	 * Fires an event on mouse click.
	 * Meant to be overridden to use mouse input in subclasses.
	 * @param button The value of the button pressed. Use Input.MOUSE_LEFT_BUTTON etc to check.
	 * @param x The X coordinate of the mouse. Starts from the left.
	 * @param y The Y coordinate of the mouse. Starts from the top.
	 * @param clickCount How many times in succession a click was made. I think it's that at least.
	 */
	protected abstract void detectMouse(int button, int x, int y);
	
	/**
	 * Fires an event when a key is pressed.
	 * Meant to be overridden to use keyboard input in subclasses.
	 * @param key The value of the key pressed. Use Input.KEY_SPACE etc to check.
	 * @param c The char representation of the key pressed. No idea how to use it.
	 */
	protected abstract void detectKey(int key, char c);
	
	protected abstract void detectMouseMove(int oldX, int oldY, int newX, int newY);
	
}
