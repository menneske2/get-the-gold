import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
/**
 *
 * @author Fredrik Foss
 */
public class MenuOptions extends Menu{
	
	public static final int sbgID = 1;
	
	String backText;
	int backPosX;
	int backPosY;
	
	public MenuOptions(){
		
	}
	
	@Override
	public int getID(){
		return sbgID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		super.init(gc, sbg);
		
		backText = "Back";
		backPosX = (gc.getWidth()-font.getWidth(backText))/2;
		backPosY = gc.getHeight()/100*30;
	}
	
	/**
	 * Fires an event on mouse click.
	 * @param button The value of the button pressed. Use Input.MOUSE_LEFT_BUTTON etc to check.
	 * @param x The X coordinate of the mouse. Starts from the left.
	 * @param y The Y coordinate of the mouse. Starts from the top.
	 * @param clickCount How many times in succession a click was made. I think it's that at least.
	 */
	@Override
	protected void detectMouse(int button, int x, int y){
		if(sbg.getCurrentStateID()==sbgID){
			if(isHoveringOverString(backText, backPosX, backPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonBack();
				}
			}
		}
	}
	
	
	/**
	 * Fires an event when a key is pressed.
	 * @param key The value of the key pressed. Use Input.KEY_SPACE etc to check.
	 * @param c The char representation of the key pressed. No idea how to use it.
	 */
	@Override
	protected void detectKey(int key, char c){
		if(sbg.getCurrentStateID()==sbgID){
			if(key == Input.KEY_ESCAPE){
				buttonBack();
			}
		}
	}
	
	@Override
	protected void detectMouseMove(int oldX, int oldY, int newX, int newY) {
		
	}
	
	@Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
    {
		g.setFont(font);
		
        g.drawString(backText, backPosX, backPosY);
		
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
    {
		
    }
	
	private void buttonBack(){
		sbg.enterState(MenuMain.sbgID);
	}

	
}
