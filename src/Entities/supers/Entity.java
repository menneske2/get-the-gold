package Entities.supers;


import java.util.HashMap;
import java.util.List;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import Levels.Level;
import Levels.AudioController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public abstract class Entity {
	
	protected Shape boundingBox = null;
	protected int posX;
	protected int posY;
	protected HashMap<String, Animation> animations;
	protected Level level;
	
	/**
	 * A genaderal entity. This layer covers position, collision detection and animation storage.
	 * @param animations A HashMap<String, Animation>. so the string "idle" would have 1 animation associated with it, "jump" has another one etc.
	 * @param boundingBox A Shape object from the Slick2D library. Has to have a whole number for X and Y.
	 */
	public Entity(HashMap<String, Animation> animations, Shape boundingBox){
		this.animations = (HashMap<String, Animation>) animations.clone();
		this.boundingBox = boundingBox;
		this.posX = (int) boundingBox.getX();
		this.posY = (int) boundingBox.getY();
	}
	
	public void setLevel(Level level){
		this.level = level;
	}
	
	public static int doubleToInt(double doub){
		return (int) Math.round(doub);
	}
	
	
	/**
	 * What the entity should do on update. Should only be called from the main game loop.
	 * @param gc The GameContainer from the parent function.
	 * @param delta The time since last frame. Also gotten from the parent function.
	 */
	public abstract void onUpdate(GameContainer gc, int delta);
	
	public abstract void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY);
	
	public void setX(int x){
		this.posX = x;
		this.getBoundingBox().setX(x);
	}
	public void setY(int y){
		this.posY = y;
		this.getBoundingBox().setY(y);
	}
	public int getPosX()
    {
        return posX;
    }
	public int getPosY(){
		return posY;
	}
	
	protected void playSound(String key){
		AudioController sound = AudioController.getAudio();
		sound.play(key);
	}
	
	protected void loadSound(String key, String path){
		AudioController sound = AudioController.getAudio();
		sound.load(key, path);
	}

	/**
	 * Gets the bounding box, which works as both a collision box, and a source of coordinates.
	 * @return 
	 */
	public Shape getBoundingBox() {
	  return this.boundingBox;
	}
	
	public void onDeath(List<Entity> entities){
		
	}
	
	/**
	 * Called when this entity collides with another.
	 * @param e The entity that collided with this one.
	 * @return Whether or not this entity died as a result of the collision.
	 */
	public boolean onCollision(Entity e){
		return false;
	}
		
	
	protected static Shape createModel(int spawnPosX, int spawnPosY, int width, int height) {
        Rectangle steveShape = new Rectangle(spawnPosX, spawnPosY, width, height); // spawnX, spawnY, width, height
        return steveShape;
    }
	
	/**
	 * Checks if this entity has collided with the specified entity
	 * @param e the entity you wish to check if this has crashed with.
	 * @return true or false
	 */
	public boolean intersects(Entity e) {
		return this.getBoundingBox().intersects(e.getBoundingBox());
	}
	
	public String hitboxCheck(Rectangle shape, int movement){
		// for performance
		if(!this.getBoundingBox().intersects(shape)){
			return null;
		}
		
		// makes it so the top part of a square always gives "top", even if it was in the "left" or "right" triangle. Also does the same for bottom.
		int boxTop = movement;	// needs to be increased if fps gets low enough or speeds get high enough.
		
		// The 4 corners of a square.
		float[] points = shape.getPoints();
		Point topLeft = new Point(points[0], points[1]);
		Point topRight = new Point(points[2], points[3]);
		Point bottomRight = new Point(points[4], points[5]);
		Point bottomLeft = new Point(points[6], points[7]);
		
		Point myTopLeft = new Point(posX, posY);
		Point myTopRight = new Point(posX + boundingBox.getWidth(), posY);
		Point myBottomLeft = new Point(posX, posY + boundingBox.getHeight());
		Point myBottomRight = new Point(posX + boundingBox.getWidth(), posY + boundingBox.getHeight());
		
		float topY = topLeft.getY();
		float bottomY = bottomLeft.getY();
		float myTopY = myTopLeft.getY();
		float myBottomY = myBottomLeft.getY();
		
		float leftX = topLeft.getX();
		float rightX = topRight.getX();
		float myLeftX = myTopLeft.getX();
		float myRightX = myTopRight.getX();
		
		
		
		
		// Handles cases where the foreign object is bigger than you. (both corners outside you)
		if(shape.contains(myTopLeft.getX(), myTopLeft.getY())){
			if(shape.contains(myTopRight.getX(), myTopRight.getY())){
				return "bottom";
			}
		}
		if(shape.contains(myBottomLeft.getX(), myBottomLeft.getY())){
			if(shape.contains(myBottomRight.getX(), myBottomRight.getY())){
				return "top";
			}
		}
		if(shape.contains(myTopLeft.getX(), myTopLeft.getY())){
			if(shape.contains(myBottomLeft.getX(), myBottomLeft.getY())){
				return "right";
			}
		}
		if(shape.contains(myTopRight.getX(), myTopRight.getY())){
			if(shape.contains(myBottomRight.getX(), myBottomRight.getY())){
				return "left";
			}
		}
		
		
		
		// Handles cases where the foreign object is tinier than you. (both corners inside you)
		if(boundingBox.contains(topLeft.getX(), topLeft.getY())){
			if(boundingBox.contains(topRight.getX(), topRight.getY())){
				return "top";
			}
		}
		if(boundingBox.contains(topLeft.getX(), topLeft.getY())){
			if(boundingBox.contains(bottomLeft.getX(), bottomLeft.getY())){
				return "left";
			}
		}
		if(boundingBox.contains(bottomLeft.getX(), bottomLeft.getY())){
			if(boundingBox.contains(bottomRight.getX(), bottomRight.getY())){
				return "bottom";
			}
		}
		if(boundingBox.contains(bottomRight.getX(), bottomRight.getY())){
			if(boundingBox.contains(topRight.getX(), topRight.getY())){
				return "right";
			}
		}
		

		// Handles the cases where only one corner connects.
		if(shape.contains(myTopLeft.getX(), myTopLeft.getY())){
			if(myTopY < bottomY - boxTop){
				if(myLeftX > rightX - boxTop){
					return "right";
				}
			}
			return "bottom";
		}
		if(shape.contains(myTopRight.getX(), myTopRight.getY())){
			if(topY < myBottomY - boxTop){
				if(myRightX < leftX + boxTop){
					return "left";
				}
			}
			return "bottom";
		}
		if(shape.contains(myBottomLeft.getX(), myBottomLeft.getY())){
			if(myBottomY > topY + boxTop){
				if(myLeftX < rightX -boxTop){
					return "right";
				}
			}
			return "top";
		}
		if(shape.contains(myBottomRight.getX(), myBottomRight.getY())){
			if(bottomY > myTopY + boxTop){
				if(myRightX < leftX + boxTop){
					return "left";
				}
			}
			return "top";
		}
		
		// to silence netbeans. will (hopefully) never be called.
		return null;
	}
	
	/**
	 * Finds out if this collided with something. returns which side it hit, from the hit things perspective.
	 * @param shape A Slick2D Shape. This function treats it like a rectangle no matter what though.
	 * @param movement The distance this entity is expected to move in the Y axis. Used to prevent glitching around.
	 * @return null if nothing was hit, "left", "right", "top" or "bottom" if something was hit.
	 */
	public String intersects(Rectangle shape, int movement){
		
		// for performance
		if(!this.getBoundingBox().intersects(shape)){
			return null;
		}
		
		// The 4 corners of a square. The center is also used in creation of the 4 side triangles.
		float[] points = shape.getPoints();
		Point topLeft = new Point(points[0], points[1]);
		Point topRight = new Point(points[2], points[3]);
		Point bottomRight = new Point(points[4], points[5]);
		Point bottomLeft = new Point(points[6], points[7]);
		
		Point myTopLeft = new Point(posX, posY);
		Point myTopRight = new Point(posX + boundingBox.getWidth(), posY);
		Point myBottomLeft = new Point(posX, posY + boundingBox.getHeight());
		Point myBottomRight = new Point(posX + boundingBox.getWidth(), posY + boundingBox.getHeight());
		
		float topY = topLeft.getY();
		float bottomY = bottomLeft.getY();
		float myTopY = myTopLeft.getY();
		float myBottomY = myBottomLeft.getY();
		
		float leftX = topLeft.getX();
		float rightX = topRight.getX();
		float myLeftX = myTopLeft.getX();
		float myRightX = myTopRight.getX();
		
		// makes it so the top part of a square always gives "top", even if it was in the "left" or "right" triangle. Also does the same for bottom.
		int boxTop = movement;	// needs to be increased if fps gets low enough or speeds get high enough.
		
		// 4 triangles. one for each side of a square.
		Polygon top = new Polygon(new float[]{topLeft.getX(), topLeft.getY(), topRight.getX(), topRight.getY(), shape.getCenterX(), shape.getCenterY()});
		Polygon left = new Polygon(new float[]{topLeft.getX(), topLeft.getY(), bottomLeft.getX(), bottomLeft.getY(), shape.getCenterX(), shape.getCenterY()});
		Polygon right = new Polygon(new float[]{bottomRight.getX(), bottomRight.getY(), topRight.getX(), topRight.getY(), shape.getCenterX(), shape.getCenterY()});
		Polygon bottom = new Polygon(new float[]{bottomRight.getX(), bottomRight.getY(), bottomLeft.getX(), bottomLeft.getY(), shape.getCenterX(), shape.getCenterY()});
		
		// If the checker has the top left point of the checkee in his hitbox.
		// It's vital that it checks "topLeft" and "topRight" before the bottom ones, or there's a high chance of glitching through the floor.
		if(this.getBoundingBox().contains(topLeft.getX(), topLeft.getY())){
			if(myBottomY > topY + boxTop){
				
				// These happen if it collides with a whole side.
				if(this.getBoundingBox().contains(topRight)){
					return "top";
				}
				if(this.getBoundingBox().contains(bottomLeft)){
					return "left";
				}
				// These happen if it collides with only one corner. 
				// Basically checks which triangle the corner is in, which is gonna be the correct one unless you're running the game at 2fps.
				if(top.contains(myBottomRight)){
					return "top";
				}
				else if(left.contains(myBottomRight)){
					return "left";
				}
				return "left";
			}
			// no matter if you hit the top or left triangle, if it's in the top part of the box, you get "top". prevents glitching.
			return "top";
		}
		
		
		// This one doesn't have any whole sides. I'm leaving those for the "bottomRight" one, so only 2 of these needs a whole sides section.
		if(this.getBoundingBox().contains(topRight.getX(), topRight.getY())){
			if(myBottomY > topY + boxTop){
				if(top.contains(myBottomLeft)){
					return "top";
				}
				if(right.contains(myBottomLeft)){
					return "right";
				}
				return "right";
			}
			return "top";
		}
		
		
		// The 2 next ones are copy pastes, except it's bottomX instead of topX. the boxTop line is also made at the bottom  instead of the top in these ones.
		
		if(this.getBoundingBox().contains(bottomRight.getX(), bottomRight.getY())){
			if(myTopY < bottomY - boxTop){
				// These happen if it collides with a whole side.
				if(this.getBoundingBox().contains(bottomLeft)){
					return "bottom";
				}
				else if(this.getBoundingBox().contains(topRight)){
					return "right";
				}
				// These happen if it collides with only one corner. 
				// Basically checks which triangle the corner is in, which is gonna be the correct one unless you're running the game at 2fps.
				if(bottom.contains(myTopLeft)){
					return "bottom";
				}
				else if(right.contains(myTopLeft)){
					return "right";
				}
				return "right";
			}
			return "bottom";
		}
		
		if(this.getBoundingBox().contains(bottomLeft.getX(), bottomLeft.getY())){
			if(myTopY < bottomY - boxTop){
				if(bottom.contains(myTopRight)){
					return "bottom";
				}
				else if(left.contains(myTopRight)){
					return "left";
				}
				return "left";
			}
			return "bottom";
		}
		// This detects quicker than the above, but is less reliable.
		// The reason this is here at all is to reduce the stuttering caused by gravity.
		float margin = 1.5f;
		// Top with myLeft:
		if(myBottomY < topY + boxTop  &&  myLeftX > leftX + margin  &&  myLeftX < rightX - margin){
			return "top";
		}
		// Top with myRight:
		if(myBottomY < topY + boxTop  &&  myRightX > leftX + margin  &&  myRightX < rightX - margin){
			return "top";
		}
		// When the foreign corners are outside of this.
		if(myBottomY < topY + boxTop  &&  myLeftX < leftX  &&  myRightX > rightX){
			return "top";
		}
		
		// Handling the cases where the player is smaller than the block.
		if(shape.contains(myTopLeft) && shape.contains(myTopRight))
			return "bottom";
		if(shape.contains(myTopLeft) && shape.contains(myBottomLeft))
			return "right";
		if(shape.contains(myTopRight) && shape.contains(myBottomRight))
			return "left";
		if(shape.contains(myBottomLeft) && shape.contains(myBottomRight))
			return "top";
		
		// to silence netbeans. will (hopefully) never be called.
		return null;
	}


}
