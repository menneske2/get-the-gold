/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.supers;

import java.util.HashMap;
import java.util.List;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Fredrik
 */
public abstract class Playable extends Damageable{
	
	private final int acceleration; //pixels/s^2
	private final int maxJumps;  // 1 allows you to jump from the ground only. 2 allows one double jump etc.
	private final int jumpStrength; // the strength of your jump in velocity aka pixels/s
	protected int drawX = 700;
	protected int drawY = 300;
    
    protected int moveDirection;
	protected int stompDamage = 10;
	protected int meleeDamage = 20;
	protected boolean isAttacking = false;
	protected int attackTimer = 0;
	
	
	public Playable(HashMap<String, Animation> animations, Shape boundingBox, int topSpeed, int MAX_HP, int acceleration, int maxJumps, int jumpStrength){
		super(animations, boundingBox, topSpeed, MAX_HP);
		this.acceleration = acceleration;
		this.maxJumps = maxJumps;
		this.jumpStrength = jumpStrength;
		facingRight = true;
	}
	
	public void setDraw(int x, int y){
		this.drawX  = x;
		this.drawY  = y;
	}
	
	public void init(GameContainer gc, StateBasedGame sbg, int sbgID){
		Input input = gc.getInput();
		KeyListener kl = new KeyListener(){
			@Override
			public void keyPressed(int key, char c) {
				if(sbg.getCurrentStateID()==sbgID){
					onKeyPressed(key, c);
				}
			}
			@Override
			public void keyReleased(int key, char c) {}
			@Override
			public void setInput(Input input) {}
			@Override
			public boolean isAcceptingInput() { return true; }
			@Override
			public void inputEnded() {}
			@Override
			public void inputStarted() {}
		};
		input.addKeyListener(kl);
	}
	
	@Override
	public void onUpdate(GameContainer gc, int delta) {
		super.onUpdate(gc, delta);

		updateAnims(delta);
		
		Input in = gc.getInput();
		moveDirection = 0;
		
		if(!isStunned){
			if(in.isKeyDown(Input.KEY_D) || in.isKeyDown(Input.KEY_RIGHT)){
				accelerate(this.acceleration, 0, false);
				moveDirection = 1;
				facingRight = true;
			}
			if(in.isKeyDown(Input.KEY_A) || in.isKeyDown(Input.KEY_LEFT)){
				accelerate(-this.acceleration, 0, false);
				moveDirection = -1;
				facingRight = false;
			}
			if(in.isKeyDown(Input.KEY_S) || in.isKeyDown(Input.KEY_DOWN)){
				accelerate(-this.acceleration, 0, false);
			}
		}
		super.applyFriction(4000, moveDirection);
		attackTimer -= delta;
		if(attackTimer <= 0) isAttacking = false;
	}
	
	/**
	 * Event based key detection. use onUpdate() if you want continuous polling.
	 * @param key The key pressed. do if(key == Input.KEY_SPACE) etc to check.
	 * @param c idk its a char i guess. probably 'c' etc.
	 */
	protected void onKeyPressed(int key, char c){
		if(!isStunned){
			if(key==Input.KEY_SPACE){
				jump(this.maxJumps, this.jumpStrength);
			}
			if(key==Input.KEY_W || key==Input.KEY_UP){
				if(!isAttacking){
					attack();
					isAttacking = true;
				}
			}
		}
	}
	
	/**
	 * Update the animations
	 * @param delta 
	 */
	protected void updateAnims(int delta){
		for(Animation anim : animations.values()){
			anim.update(delta);
		}
	}
	
	@Override
	public void onDeath(List<Entity> entities){
		super.onDeath(entities);
	}
	
	/**
	 * Deploys a melee attack.
	 */
	protected abstract void attack();
	
	/**
	 * Sets the base stomp damage. Default value is 10.
	 * @param amount
	 */
	protected void setStompDamage(int amount){
		stompDamage = amount;
	}
	
	/**
	 * Gets the stomp damage with all damage modifiers.
	 * @return 
	 */
	public int getStompDamage(){
		return stompDamage;
	}
	
	/**
	 * Sets the base melee damage. Default value is 20.
	 * @param amount
	 */
	protected void setMeleeDamage(int amount){
		meleeDamage = amount;
	}
	
	/**
	 * Gets the melee damage with all damage modifiers.
	 * @return 
	 */
	public int getMeleeDamage(){
		return meleeDamage;
	}
	
	public int getJumpPower(){
		return jumpStrength;
	}
	
}
