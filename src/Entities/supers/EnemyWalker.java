/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.supers;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Shape;

/**
 *
 * @author Fredrik Foss
 */
public abstract class EnemyWalker extends Damageable{
	
	public EnemyWalker(HashMap<String, Animation> animations, Shape boundingBox, int topSpeed, int MAX_HP){
		super(animations, boundingBox, topSpeed, MAX_HP);
	}
	
	@Override
	public void onUpdate(GameContainer gc, int delta) {
		super.onUpdate(gc, delta);
		
		int direction = facingRight ? 1 : -1;
		int searchDistanceX = direction * 20;
		if(facingRight) searchDistanceX += (int)this.getBoundingBox().getWidth();
		int searchDistanceY = (int)this.getBoundingBox().getHeight() + 8;
		
		if(!level.isSolidBlockAt(posX + searchDistanceX, posY + searchDistanceY)){
			facingRight = !facingRight;
		}
		
		this.accelerate(direction*500, 0, false);
	}
	
	
	@Override
	public void onWallCollision(String side){
		if(side.equals("left")) facingRight = false;
		else if(side.equals("right")) facingRight = true;
	}

}
