/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.supers;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Shape;

/**
 *
 * @author Fredrik
 */
public abstract class Damageable extends Mobile{
	
	protected int hpMax;
	protected int hp;
	
	private final int IFRAMES_ON_HIT = 500;
	private int iFramesRemaining = 0;
	
	public Damageable(HashMap<String, Animation> animations, Shape boundingBox, int topSpeed, int MAX_HP){
		super(animations, boundingBox, topSpeed);
		this.hpMax = MAX_HP;
		this.hp = hpMax;
	}
	
	@Override
	public void onUpdate(GameContainer gc, int delta) {
		super.onUpdate(gc, delta);
		iFramesRemaining -= delta;
	}

	/**
	 * Damages this creature.
	 * @param amount The damage this should take.
	 * @return whether or not the damage was applied.
	 */
	public boolean damage(int amount){
		if(iFramesRemaining > 0) return false;
		hp -= amount;
		iFramesRemaining = IFRAMES_ON_HIT;
		if(hp<=0) level.removeEntity(this);
		return true;
	}
	
	/**
	 * Makes this creature immune to damage for an amount of time. Does not stack.
	 * @param duration How long the effect should last in ms
	 */
	public void setInvulnerable(int duration){
		if(iFramesRemaining > duration) return; // Makes it so you can't override a better invulnerability.
		iFramesRemaining = duration;
	}
	
	/**
	 * Sets the max hp of this entity, then heals it to that value.
	 * @param num 
	 * @return this entity.
	 */
	public Entity setMaxHP(int num){
		this.hpMax = num;
		this.hp = num;
		return this;
	}
	
	public int getHP(){
		return hp;
	}
}
