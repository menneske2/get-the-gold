/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.supers;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Shape;

/**
 *
 * @author Fredrik Foss
 */
public abstract class EnemyFlying extends Damageable{
	
	private int timePerCycle = 1500;
	private int amplitude = 5;
	private int cycleTimer = 0;
	private int territoryMiddle;
	private int territoryReach = 300;
	
	public EnemyFlying(HashMap<String, Animation> animations, Shape boundingBox, int topSpeed, int MAX_HP){
		super(animations, boundingBox, topSpeed, MAX_HP);
		setGravity(0);
		this.territoryMiddle = posX;
	}
	
	@Override
	public void onUpdate(GameContainer gc, int delta) {
		super.onUpdate(gc, delta);
		
		// Sinus wave hovering.
		cycleTimer = ((cycleTimer + delta) % timePerCycle); // Updating and keeping it within limits
		double converted = cycleTimer * 2*Math.PI / timePerCycle;// * Math.PI/180; // converting to radians
		converted = amplitude * Math.sin(converted); // more converting.
		int moveDir = facingRight ? 1 : -1;
		this.applyFriction(4000, moveDir);
		setVeloY(getVeloY() * Math.pow(0.95, delta));
		upVelo(0, converted);
		
		// Makes sure it doesn't leave its territory.
		if(posX > territoryMiddle + territoryReach) facingRight = false;
		if(posX < territoryMiddle - territoryReach) facingRight = true;
		
		int direction = facingRight ? 1 : -1;
		this.accelerate(direction*200, 0, true);
	}
	
	@Override
	public void onWallCollision(String side){
		if(side.equals("left")) facingRight = false;
		else if(side.equals("right")) facingRight = true;
	}
	
	/**
	 * Sets the constants for the sinus wave hovering.
	 * @param amplitude how far above/below middlepoint it should go. not in any real units, but 5 is default and its exponential.
	 * @param timePerCycle how long it should take to do a full up n down on the sinus wave.
	 */
	protected void setFlyingPattern(int amplitude, int timePerCycle){
		this.amplitude = amplitude;
		this.timePerCycle = timePerCycle;
	}
	
	/**
	 * Sets how far this entity is allowed to roam. default is 300.
	 * @param middlePoint the middlepoint of its territory.
	 * @param reach the distance from spawn X it is allowed to roam.
	 */
	protected void setTerritory(int middlePoint, int reach){
		this.territoryMiddle = middlePoint;
		this.territoryReach = reach;
	}
}
