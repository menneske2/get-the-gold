/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.supers;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Shape;

/**
 *
 * @author Fredrik
 */
public abstract class Mobile extends Entity{
	
	private int jumpCount = 99; // keeps track of how many jumps have been done while in air. starts at 99 so you can't jump from spawn.
	private final int topSpeed;
	
	
	private int gravity = 981; // piksler per sekund
	private double frictionConstant = 0.2;
	private final int STUN_ON_DAMAGE = 500;
	
	// Statuses
	protected boolean airborne = true;
	private double airborneTimeout = 0;
	protected boolean isStunned = false;
	private double stunTimeout = 0;
	protected boolean facingRight = false; // facing direction
	
	// Acceleration and velocity. begge er i piksler per sekund
	private double accelX = 0;
	private double accelY = 0;
	protected double veloX = 0;
	protected double veloY = 0;
	// Movepoints er trengt for å hindre den i å runde ned til 0 ved høyere fps.
	private double movePointsX = 0;
	private double movePointsY = 0;
	
	
	/**
	 * The layer below Entity. This one takes care of physics and general movement abilities.
	 * @param animations A HashMap<String, Animation>. so the string "idle" would have 1 animation associated with it, "jump" has another one etc.
	 * @param boundingBox A Shape object from the Slick2D library. Has to have a whole number for X and Y.
	 * @param topSpeed The maximum movement speed of the entity.
	 */
	public Mobile(HashMap<String, Animation> animations, Shape boundingBox, int topSpeed){
		super(animations, boundingBox);
		this.topSpeed = topSpeed;
	}
	
	
	
	@Override
	public void onUpdate(GameContainer gc, int delta){
		
		// makes it so falling from a cliff doesnt give you an extra doublejump.
		airborneTimeout -= delta;
		stunTimeout -= delta;
		if(airborneTimeout<0) airborneTimeout=0;
		if(stunTimeout < 0) isStunned = false;
		
		accelerate(0, gravity, true); // applies the gravity.
		move(delta);
		
		// sets acceleration to 0, which allows multiple sources to influence acceleration per frame.
		accelX = 0;
		accelY = 0;
		this.getBoundingBox().setX(posX);
		this.getBoundingBox().setY(posY);
	}
	
	/**
	 * Not meant to be called by subclasses. use super.onUpdate() instead.
	 * Executes a tick of regular newtonian movement.
	 * @param delta Time since last frame. Supplied by the update() function.
	 */
	private void move(int delta){
		
		veloX += accelX * delta/1000f;
		veloY += accelY * delta/1000f;
		if(veloX>topSpeed) veloX=topSpeed;
		if(veloX<-topSpeed) veloX=-topSpeed;
		movePointsX += veloX * delta/1000f;
		movePointsY += veloY * delta/1000f;
		
		//  alt dette køddet er fordi posX/posY MÅ være int for graphics. kan ikke caste til int, for da blir det runda ned til 0.
		int tempX = 0;
		int tempY = 0;
		while(movePointsX >= 1 || movePointsX <= -1){
			if(movePointsX>0){
				tempX++;
				movePointsX--;
			} else{
				tempX--;
				movePointsX++;
			}
		}
		while(movePointsY >= 1 || movePointsY <= -1){
			if(movePointsY>0){
				tempY++;
				movePointsY--;
			} else{
				tempY--;
				movePointsY++;
			}
		}
		
		// flytter faktisk
		this.posX += tempX;
		this.posY += tempY;
	}
	
	/**
	 * Applies friction based on the weight of the character and its direction of movement.
	 * @param weight The weight of your character. recommended value is 4000.
	 * @param moveDir The direction you're currently accelerating in. 1 = right. -1 = left. 0 = neutral
	 */
	protected void applyFriction(int weight, int moveDir){
		int force = (int) Math.ceil(weight*frictionConstant);
		int direction = veloX>0 ? 1 : -1;
		boolean isMovingAlong = (moveDir==direction && !isStunned);
		if(isMovingAlong) force /= 6;
		if(airborne) force = 133;
		accelerate(-direction*force, 0, true);
		
		// Makes sure the character stops movement when standing still instead of slowly moving in one direction.
		if(Math.abs(veloX) < 0.2 && moveDir==0){
			veloX = 0;
			accelX = 0;
		}
	}
	
	/**
	 * Adds acceleration in pixels/s^2. Must be applied every frame
	 * @param x The X axis. Left to right.
	 * @param y The Y axis. Top to bottom.
	 * @param forced Whether or not the movement is forced. If it's forced, it happens no matter what. If it's not forced, stunning disables the accelleration.
	 */
	public void accelerate(double x, double y, boolean forced){
		if(!isStunned || forced){
			accelX += x;
			accelY += y;
		}
	}
	public void upVelo(double x, double y){
		veloX += x;
		veloY += y;
	}
	public void translate(double x, double y){
		movePointsX += x;
		movePointsY += y;
	}
	public void setVeloY(double y){
		veloY = y;
	}
	public void setVeloX(double x){
		veloX = x;
	}
	public double getAccelY(){
		return accelY;
	}
	public double getAccelX(){
		return accelX;
	}
	public double getVeloX(){
		return veloX;
	}
	public double getVeloY(){
		return veloY;
	}
	
	
	protected void jump(int maxJumps, int jumpStrength){
		if(jumpCount < maxJumps){
			this.setVeloY(-jumpStrength);
			jumpCount++;
			airborne = true;
		}
	}
	
	public void onLanding(double frictionConstant){
		jumpCount = 0;
		airborne = false;
		airborneTimeout = 100;
		this.frictionConstant = frictionConstant;
	}
	
	public void onLiftoff(){
		if(!airborne && airborneTimeout==0){
			jumpCount++;
			airborne = true;
		}
	}
	
	/**
	 * Do something when you collide with a wall.
	 * @param side the side you hit. "left" means that you were moving right, and thus hit the left part.
	 */
	public void onWallCollision(String side){
		
	}
	
	/**
	 * Knocks this entity back (or forward) and stuns it.
	 * @param angle The angle to launch at in degrees. 0 is straight back, 90 is upwards etc. 45 reccommended.
	 * @param knockerPosX Used to figure out which direction to knock towards. The knockee will be pushed the opposite direction of the knocker.
	 * @param force The force at which to launch this entity back in m/s
	 * @param stunModifier The stun modifier. 0 for no stun, 1.1 for 10% more stun time etc.
	 */
	public void knockback(int angle, int knockerPosX, int force, double stunModifier){
		double angleRad = angle/180.0 * Math.PI;
		int direction = knockerPosX>posX ? -1 : 1; // this is "flipped" so that a 45 degree angle goes away instead of in.
		if(angle%180 != 90) this.setVeloX(direction * Math.cos(angleRad) * force); // If its straight up/down, dont change x velocity.
		if(angle%180 != 0) this.setVeloY(- Math.sin(angleRad) * force); // - because y goes from top to bottom.
		if(stunModifier != 0) stun((int) Math.round(stunModifier * STUN_ON_DAMAGE));
	}
	
	/**
	 * Stuns this entity for a duration of time.
	 * @param duration The amount of time this should be stunned for in ms.
	 */
	public void stun(int duration){
		isStunned = true;
		if(duration > stunTimeout) // Makes sure we dont replace a strong stun with a tiny one.
			stunTimeout = duration;
	}
	
	/**
	 * Sets the gravity constant. Default is 981.
	 * @param gravity 
	 */
	public void setGravity(int gravity){
		this.gravity = gravity;
	}
}
