/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import Entities.supers.Immobile;
import java.util.HashMap;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import Entities.supers.Damageable;
import org.newdawn.slick.Color;

/**
 *
 * @author Fredrik
 */
public class Hitbox extends Immobile{
	
	private final int DAMAGE;
	private final Entity SOURCE;
	private int lifespan;
	
	public Hitbox(Shape area, int damage, int lifespan, Entity source){
		super(new HashMap<>(), area);
		this.DAMAGE = damage;
		this.lifespan = lifespan;
		this.SOURCE = source;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {
		lifespan -= delta;
		if(lifespan<=0) level.removeEntity(this);
	}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
//		g.setColor(Color.red);
//		g.fillRoundRect((float)posX-offsetX, (float)posY-offsetY, boundingBox.getWidth(), boundingBox.getHeight(), 99);
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(e instanceof Damageable && e != SOURCE){
			Damageable mob = (Damageable) e;
			mob.damage(DAMAGE);
			mob.knockback(30, posX, DAMAGE*30, 1);
			return true;
		}
		return false;
	}
	
	public Entity getSource(){
		return SOURCE;
	}
	
	public int getDamage(){
		return DAMAGE;
	}
	
}
