/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import Entities.supers.Immobile;
import Entities.supers.Mobile;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

/**
 *
 * @author Fredrik
 */
public class Spring extends Immobile{
	
	private static final String FOLDER = "resource/general/spring";
	private final int power;
	
	/**
	 * Creates a 32x32 spike at the target position in the grid.
	 * @param posX the location in the grid. NOT absolute coordinates.
	 * @param posY the location in the grid. NOT absolute coordinates.
	 * @param power the velocity it should launch the player upwards.
	 * @throws SlickException 
	 */
	public Spring(int posX, int posY, int power) throws SlickException{
		super(getAnimations(), Entity.createModel(posX*32, posY*32, 32, 32));
		this.power = power;
		this.loadSound("spring", FOLDER+"/boing.wav");
	}
	
	private static HashMap<String, Animation> getAnimations() throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		
		Image normal = new Image(FOLDER+"/normal.png");
		animations.put("normal", new Animation(new SpriteSheet(normal, 32, 32), 9000));
		
		return animations;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		animations.get("normal").draw(posX-offsetX, posY-offsetY);
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(e instanceof Mobile){
			Mobile mob = (Mobile) e;
			
			// So they dont get penetrated.
			String hitPart = this.intersects((Rectangle) e.getBoundingBox(), 3);
			if(hitPart == null) return false;
			switch(hitPart){
				case "bottom":
					if (mob.getVeloY() > 0) mob.setVeloY(-power);
					mob.setY(posY - (int)mob.getBoundingBox().getHeight());
					mob.onLanding(0.2);
					this.playSound("spring");
					break;
			}
		}
		return false;
	}
}
