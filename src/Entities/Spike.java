/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Damageable;
import Entities.supers.Entity;
import Entities.supers.Immobile;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

/**
 *
 * @author Fredrik
 */
public class Spike extends Immobile{
	
	private static final String FOLDER = "resource/general/spike";
	private boolean bloodied = false;
	
	private final int DAMAGE = 10;
	
	/**
	 * Creates a 32x32 spike at the target position in the grid.
	 * @param posX the location in the grid. NOT absolute coordinates.
	 * @param posY the location in the grid. NOT absolute coordinates.
	 * @throws SlickException 
	 */
	public Spike(int posX, int posY) throws SlickException{
		super(getAnimations(), Entity.createModel(posX*32, posY*32, 32, 32));
	}
	
	private static HashMap<String, Animation> getAnimations() throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		
		Image normal = new Image(FOLDER+"/normal.png");
		Image bloody = new Image(FOLDER+"/bloody.png");
		
		animations.put("normal", new Animation(new SpriteSheet(normal, 32, 32), 9000));
		animations.put("bloody", new Animation(new SpriteSheet(bloody, 32, 32), 9000));
		
		return animations;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(bloodied)
			animations.get("bloody").draw(posX-offsetX, posY-offsetY);
		else
			animations.get("normal").draw(posX-offsetX, posY-offsetY);
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(e instanceof Damageable){
			Damageable mob = (Damageable) e;
			
			if(mob.getVeloX()>100) mob.setVeloX(100);
			this.bloodied = true;
			
			// So they dont get penetrated.
			String hitPart = this.intersects((Rectangle) e.getBoundingBox(), 3);
			if(hitPart == null) return false;
			switch(hitPart){
				case "bottom":
					if (mob.getVeloY() > 0) mob.setVeloY(0);
					mob.setY(posY - (int)mob.getBoundingBox().getHeight());
					mob.damage(DAMAGE);
					mob.onLanding(0.7);
					break;
					
				case "top":
					if (mob.getVeloY() < 0) mob.setVeloY(0);
					mob.setY(posY + (int)this.getBoundingBox().getHeight());
					break;
					
				case "left":
					if (mob.getVeloX() < 0) mob.setVeloX(0);
					mob.setX(posX + (int)this.getBoundingBox().getWidth());
					break;
					
				case "right":
					if (mob.getVeloX() > 0) mob.setVeloX(0);
					mob.setX(posX - (int)mob.getBoundingBox().getWidth());
					break;
			}
		}
		return false;
	}
}
