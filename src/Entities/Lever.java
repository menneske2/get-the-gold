/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import Entities.supers.Immobile;
import Entities.supers.Playable;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author Fredrik
 */
public class Lever extends Immobile{
	
	private static final String FOLDER = "resource/general/lever";
	private boolean active = false;
	private long toggleTimeout = 0;	
	/**
	 * Creates a 32x32 lever at the target position in the grid.
	 * @param posX the location in the grid. NOT absolute coordinates.
	 * @param posY the location in the grid. NOT absolute coordinates.
	 * @throws SlickException 
	 */
	public Lever(int posX, int posY) throws SlickException{
		super(getAnimations(), Entity.createModel(posX*32, posY*32, 32, 32));
		this.loadSound("lever", FOLDER+"/lever.wav");
	}
	
	private static HashMap<String, Animation> getAnimations() throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		
		Image off = new Image(FOLDER+"/off.png");
		Image on = new Image(FOLDER+"/on.png");
		animations.put("off", new Animation(new SpriteSheet(off, 32, 32), 9000));
		animations.put("on", new Animation(new SpriteSheet(on, 32, 32), 9000));
		
		return animations;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {
		toggleTimeout--;
	}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(active) 
			animations.get("on").draw(posX-offsetX, posY-offsetY);
		else
			animations.get("off").draw(posX-offsetX, posY-offsetY);
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(e instanceof Hitbox){
			if(((Hitbox) e).getSource() instanceof Playable){
				toggle();
			}
		}
		return false;
	}
	
	public boolean isOn(){
		return active;
	}
	
	public void toggle(){
		if(toggleTimeout <= 0){
			this.active = !this.active;
			toggleTimeout = 120;
			this.playSound("lever");
		}
			
	}
	
	public void setOn(){
		this.active = true;
	}
	
	public void setOff(){
		this.active = false;
	}
}