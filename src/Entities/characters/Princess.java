/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.characters;

import Entities.Hitbox;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;

/**
 *
 * @author Fredrik Og Rene
 */
public class Princess extends Entities.supers.Playable {

    // Physics properties
    private static final int TOP_SPEED = 300;  // pixels/s
    private static final int ACCELERATION = 500; //pixels/s^2
    private static final int MAX_JUMPS = 2;  // 1 allows you to jump from the ground only. 2 allows one double jump etc.
    private static final int JUMP_STRENGTH = 450; // the strength of your jump in velocity aka pixels/s
	
	private static final int MAX_HP = 40;
	private static final int ATTACK_ANIM_LENGTH = 500;
			
	// Graphics
	public static final String FOLDER = "resource/characters/princess";
	private static final int CHAR_WIDTH = 33; // The sprite is 32 wide, but she can't be as wide as the tile width.
	private static final int CHAR_HEIGHT = 54;
	

    public Princess(int spawnPosX, int spawnPosY) throws SlickException {
        super(getAnimations(CHAR_WIDTH, CHAR_HEIGHT), Entities.supers.Entity.createModel(spawnPosX, spawnPosY, CHAR_WIDTH, CHAR_HEIGHT),
                TOP_SPEED, MAX_HP, ACCELERATION, MAX_JUMPS, JUMP_STRENGTH);
		this.loadSound("PrincessHurt", FOLDER+"/hurt.wav");
		this.loadSound("PrincessHurt2", FOLDER+"/hurt2.wav");
		this.loadSound("PrincessAttack", FOLDER+"/attack.wav");
    }

    private static HashMap<String, Animation> getAnimations(int width, int height) throws SlickException {
        HashMap<String, Animation> animations = new HashMap<>();

		Image walk = new Image(FOLDER+"/walk.png").getScaledCopy(width * 4, height);
		Image idle = new Image(FOLDER+"/idle.png").getScaledCopy(width, height);
		Image jump = new Image(FOLDER+"/jump.png").getScaledCopy(width, height);
		Image meleeAttack = new Image(FOLDER+"/meleeAttack.png").getScaledCopy(width * 5, height);
		Image hurt = new Image(FOLDER+"/hurt.png").getScaledCopy(width, height);
		animations.put("idle", new Animation(new SpriteSheet(idle, width, height), 100));
		animations.put("walk", new Animation(new SpriteSheet(walk, width, height), 100));
		animations.put("jump", new Animation(new SpriteSheet(jump, width, height), 100));
		animations.put("meleeAttack", new Animation(new SpriteSheet(meleeAttack, width, height), 100));
		animations.put("hurt", new Animation(new SpriteSheet(hurt, width, height), 100));
        return animations;
    }


    @Override
    public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(isStunned) {
            animations.get("hurt").getCurrentFrame().getFlippedCopy(!facingRight, false).draw(drawX, drawY);
        }
		else if(isAttacking){
			animations.get("meleeAttack").getCurrentFrame().getFlippedCopy(!facingRight, false).draw(drawX, drawY);
		}
		else if(this.airborne == true){
			animations.get("jump").getCurrentFrame().getFlippedCopy(!facingRight, false).draw(drawX, drawY);
		}
		else if(this.moveDirection == 0) {
            animations.get("idle").getCurrentFrame().getFlippedCopy(!facingRight, false).draw(drawX, drawY);
        }
		else if(this.moveDirection != 0) {
            animations.get("walk").getCurrentFrame().getFlippedCopy(!facingRight, false).draw(drawX, drawY);
        }
    }

	@Override
	public boolean damage(int dmg){
		boolean ayeornay = super.damage(dmg);
		if(ayeornay){
			if(Math.random() < 0.5) this.playSound("PrincessHurt");
			else this.playSound("PrincessHurt2");
		}
		return ayeornay;	
	}
	
	@Override
	protected void attack() {
		// TODO: Create a damaging hitbox with a delay for when it starts damaging. delay in hitbox constructor.
		int hboxDistance = facingRight ? (int)boundingBox.getWidth()+40 : -40;
		Shape hurtArea = new Ellipse(posX+hboxDistance, posY + boundingBox.getHeight()/2, 40, 30);
		Hitbox hitbox = new Hitbox(hurtArea, meleeDamage, 100, this);
		level.addEntity(hitbox);
		attackTimer = ATTACK_ANIM_LENGTH;
		this.playSound("PrincessAttack");
	}

}
