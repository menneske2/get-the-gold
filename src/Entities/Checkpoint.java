/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import Entities.supers.Immobile;
import Entities.supers.Playable;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author Fredrik
 */
public class Checkpoint extends Immobile{
	
	private static final String FOLDER = "resource/general/checkpoint";
	private static final int WIDTH = 64;
	private static final int HEIGHT = 64;
	
	private boolean active = false;
	
	
	/**
	 * Creates a 32x32 spike at the target position in the grid.
	 * @param posX the location in the grid. NOT absolute coordinates.
	 * @param posY the location in the grid. NOT absolute coordinates.
	 * @throws SlickException 
	 */
	public Checkpoint(int posX, int posY) throws SlickException{
		super(getAnimations(), Entity.createModel(posX, posY, WIDTH, HEIGHT));
		this.loadSound("checkpoint", FOLDER+"/checkpoint.wav");
	}
	
	private static HashMap<String, Animation> getAnimations() throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		
		Image inactive = new Image(FOLDER+"/inactive.png");
		Image active = new Image(FOLDER+"/active.png");
		animations.put("inactive", new Animation(new SpriteSheet(inactive, WIDTH, HEIGHT), 9000));
		animations.put("active", new Animation(new SpriteSheet(active, WIDTH, HEIGHT), 9000));
		
		return animations;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(active) animations.get("active").draw(posX-offsetX, posY-offsetY);
		else animations.get("inactive").draw(posX-offsetX, posY-offsetY);
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(!active && e instanceof Playable){
			active = true;
			level.deactivateOtherCheckpoints(this);
			level.setCheckpoint(posX, posY);
			this.playSound("checkpoint");
		}
		return false;
	}
	
	public boolean isActive(){
		return active;
	}
	
	public void setEnabled(boolean yes){
		active = yes;
	}
	
}
