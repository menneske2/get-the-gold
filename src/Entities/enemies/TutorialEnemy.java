/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.enemies;

import Entities.supers.Entity;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

/**
 *
 * @author Fredrik
 */
public class TutorialEnemy extends Entities.supers.Damageable{
	
	private static final int WIDTH = 96;
	private static final int HEIGHT = 96;
	private static final String FOLDER = "resource/enemies/business guru/";
	
	private static final int TOP_SPEED = 0;
	private static final int MAX_HP = 30;
	private static final int KNOCKBACK_STRENGTH = 600;
	
	public boolean stompImmune = false;
	
	
	public TutorialEnemy(int x, int y, double scale) throws SlickException{
		super(getAnimations(Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), TOP_SPEED, MAX_HP);
		this.loadSound("tutorialEnemyStomped", FOLDER+"/stomped.wav");
	}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(stompImmune)
			animations.get("stompImmune").draw(posX - offsetX, posY - offsetY);
		else
			animations.get("idle").draw(posX - offsetX, posY - offsetY);
	}
	
	private static HashMap<String, Animation> getAnimations(int width, int height) throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		Image idle = new Image(FOLDER + "idle.png").getScaledCopy(width, height);
		animations.put("idle", new Animation(new SpriteSheet(idle, width, height), 100));
		Image stompImmune = new Image(FOLDER+"stompImmune.png").getScaledCopy(width, height);
		animations.put("stompImmune", new Animation(new SpriteSheet(stompImmune, width, height), 100));
		return animations;
	}
	
	@Override
    public boolean onCollision(Entities.supers.Entity e)
    {
		super.onCollision(e);
		if(!(e instanceof Entities.supers.Playable)) return false; // do nothing if it didnt collide with a player.
		
		String hitPart = this.intersects((Rectangle) e.getBoundingBox(), (int) Math.ceil(this.getVeloY())*2);
		// this detects things a frame later than the other intersects function, so it has a chance of being null.		
		if(hitPart == null) return false;
		Entities.supers.Playable player = (Entities.supers.Playable) e;
		switch(hitPart){
			case "bottom": // The players feet hit this before any other part.
				player.knockback(90, posX, player.getJumpPower()/10*11, 0);
				player.onLanding(0.2);
				if(!stompImmune){
					if(this.damage(player.getStompDamage()))
						this.playSound("tutorialEnemyStomped");
				}
				break;
			case "top":
				player.knockback(270, posX, KNOCKBACK_STRENGTH, 1);
				break;
			case "left":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				break;
			case "right":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
		}
		return false;
    }  
	
}
