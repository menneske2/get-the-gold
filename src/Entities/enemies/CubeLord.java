/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.enemies;


import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import Entities.supers.Entity;

/**
 *
 * @author Henrik
 */
public class CubeLord extends Entities.supers.EnemyWalker
{

    private static final int WIDTH = 100;
    private static final int HEIGHT = 100;
	private final int KNOCKBACK_STRENGTH = 900;
	private static final String ENEMIES_FOLDER = "resource/enemies/";

	
    public CubeLord(String enemyFolder,int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+enemyFolder, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 150, 40);
    }
	
	public CubeLord(int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+"CubeLord", Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 150, 40);
    }

    @Override
    public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY)
    {
		if (veloX >= 0){
            animations.get("walkLeftSprite").draw(posX - offsetX, posY - offsetY);
        }
		else if (veloX < 0){
            animations.get("walkRightSprite").draw(posX - offsetX, posY - offsetY);
        }
    }



    private static HashMap<String, Animation> getAnimations(String enemyFolder, int width, int height) throws SlickException
    {
        HashMap<String, Animation> animations = new HashMap<>();
        // 1 block is 16x16 a character is 1 block width and 2 blocks height (16x32)
        // spriteSheets have been made in 4 different animations
		
		Image idle = new Image(enemyFolder + "/idle.png").getScaledCopy(width, height);
		Image walkL = new Image(enemyFolder + "/walkLeft.png").getScaledCopy(width, height);
		Image walkR = new Image(enemyFolder + "/walkRight.png").getScaledCopy(width, height);

		animations.put("walkRightSprite", new Animation(new SpriteSheet(walkL, width, height), 100));
		animations.put("normalSprite", new Animation(new SpriteSheet(idle, width, height), 100));
		animations.put("walkLeftSprite", new Animation(new SpriteSheet(walkR, width, height), 100));
            
        return animations;
    }
    
    
    @Override
    public boolean onCollision(Entities.supers.Entity e)
    {
		super.onCollision(e);
		if(!(e instanceof Entities.supers.Playable)) return false; // do nothing if it didnt collide with a player.
		
		String hitPart = this.intersects((Rectangle) e.getBoundingBox(), (int) Math.ceil(this.getVeloY())*2);
		// this detects things a frame later than the other intersects function, so it has a chance of being null.		
		if(hitPart == null) return false;

		Entities.supers.Playable player = (Entities.supers.Playable) e;
		switch(hitPart){
			case "bottom": // The players feet hit this before any other part.
				player.knockback(90, posX, player.getJumpPower()/10*7, 0);
				this.damage(player.getStompDamage());
			case "top":
				player.knockback(270, posX, KNOCKBACK_STRENGTH, 1);
				break;
			case "left":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				facingRight = false;
				break;
			case "right":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				facingRight = true;
		}
		return false;
    }  
	
}
