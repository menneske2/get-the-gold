/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.enemies;


import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import Entities.supers.Entity;
import Entities.supers.Playable;

/**
 *
 * @author Henrik
 */
public class LizardBoi extends Entities.supers.EnemyWalker
{

    private static final int WIDTH = 60;
    private static final int HEIGHT = 33;
	private final int KNOCKBACK_STRENGTH = 500;
	private static final String ENEMIES_FOLDER = "resource/enemies/";
	private static final int TOP_SPEED = 150;
	private static final int MAX_HP = 40;
	
	private String FOLDER;
	private int barkTimeout = 0;

	
    public LizardBoi(String enemyFolder,int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+enemyFolder, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), TOP_SPEED, MAX_HP);
		FOLDER = ENEMIES_FOLDER+enemyFolder;
		this.loadSound("lizardBoiStomped", FOLDER+"/stomp.wav");
    }
	
	public LizardBoi(int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+"lizardBoi", Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), TOP_SPEED, MAX_HP);
		FOLDER = ENEMIES_FOLDER+"lizardBoi";
    }

    @Override
    public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY)
    {
		if (veloX >= 0){
            animations.get("walkLeftSprite").draw(posX - offsetX, posY - offsetY);
        }
		else if (veloX < 0){
            animations.get("walkRightSprite").draw(posX - offsetX, posY - offsetY);
        }
    }




    private static HashMap<String, Animation> getAnimations(String enemyFolder, int width, int height) throws SlickException
    {
        HashMap<String, Animation> animations = new HashMap<>();
        // 1 block is 16x16 a character is 1 block width and 2 blocks height (16x32)
        // spriteSheets have been made in 4 different animations
		
		Image idle = new Image(enemyFolder + "/idle.png").getScaledCopy(width, height);
		Image walkL = new Image(enemyFolder + "/walkLeft.png").getScaledCopy(width * 8, height);
		Image walkR = new Image(enemyFolder + "/walkRight.png").getScaledCopy(width * 8, height);

		animations.put("walkRightSprite", new Animation(new SpriteSheet(walkL, width, height), 100));
		animations.put("normalSprite", new Animation(new SpriteSheet(idle, width, height), 100));
		animations.put("walkLeftSprite", new Animation(new SpriteSheet(walkR, width, height), 100));
            
        return animations;
    }
    
    
    @Override
    public boolean onCollision(Entities.supers.Entity e)
    {
		super.onCollision(e);
		if(!(e instanceof Playable)) return false; // do nothing if it didnt collide with a player.
		
		String hitPart = this.hitboxCheck((Rectangle) e.getBoundingBox(), (int) Math.ceil(this.getVeloY())*2);
		// this detects things a frame later than the other intersects function, so it has a chance of being null.	
		if(hitPart == null) return false;
		Playable player = (Playable) e;
		switch(hitPart){
			case "bottom": // The players feet hit this before any other part.
				player.knockback(90, posX, player.getJumpPower()/10*11, 0);
				this.upVelo(0, 600);
				player.onLanding(0.2);
				if(this.damage(player.getStompDamage()))
					this.playSound("lizardBoiStomped");
				break;
			case "top":
				player.knockback(270, posX, KNOCKBACK_STRENGTH, 1);
				this.upVelo(0, -600);
				player.damage(10);
				break;
			case "left":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				this.upVelo(-600, 0);
				player.damage(10);
				facingRight = false;
				break;
			case "right":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				this.upVelo(600, 0);
				player.damage(10);
				facingRight = true;
		}
		return false;
    }  
	
}
