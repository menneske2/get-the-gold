/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.enemies;


import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import Entities.supers.Entity;

/**
 *
 * @author Henrik
 */
public class Wasp extends Entities.supers.EnemyFlying
{

    private static final int WIDTH = 47;
    private static final int HEIGHT = 46;
	private final int KNOCKBACK_STRENGTH = 500;
	private static final String ENEMIES_FOLDER = "resource/enemies/";
	
	private String FOLDER;

	
    public Wasp(String enemyFolder,int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+enemyFolder, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 0, 20);
		FOLDER = ENEMIES_FOLDER+enemyFolder;
		this.loadSound("waspStomped", FOLDER+"/stomp.wav");
    }
	
	public Wasp(int x, int y, double scale) throws SlickException
    {
        super(getAnimations(ENEMIES_FOLDER+"wasp", Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 
				Entity.createModel(x, y, Entity.doubleToInt(WIDTH*scale), Entity.doubleToInt(HEIGHT*scale)), 0, 20);
		FOLDER = ENEMIES_FOLDER+"wasp";
    }

    @Override
    public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY)
    {
		animations.get("idle").draw(posX - offsetX, posY - offsetY);
    }



    private static HashMap<String, Animation> getAnimations(String enemyFolder, int width, int height) throws SlickException
    {
        HashMap<String, Animation> animations = new HashMap<>();
        // 1 block is 16x16 a character is 1 block width and 2 blocks height (16x32)
        // spriteSheets have been made in 4 different animations
		
		Image idle = new Image(enemyFolder + "/idle.png").getScaledCopy(width*6, height);

		animations.put("idle", new Animation(new SpriteSheet(idle, width, height), 30));
            
        return animations;
    }
    
    
    @Override
    public boolean onCollision(Entities.supers.Entity e)
    {
		super.onCollision(e);
		if(!(e instanceof Entities.supers.Playable)) return false; // do nothing if it didnt collide with a player.
		
		String hitPart = this.hitboxCheck((Rectangle) e.getBoundingBox(), (int) Math.ceil(this.getVeloY())*2);
		System.out.println(hitPart);
		// this detects things a frame later than the other intersects function, so it has a chance of being null.		
		if(hitPart == null) return false; 

		Entities.supers.Playable player = (Entities.supers.Playable) e;
		switch(hitPart){
			case "bottom": // The players feet hit this before any other part.
				player.knockback(90, posX, player.getJumpPower()/10*11, 0);
				player.translate(0, -15);
				player.onLanding(0.2);
				if(this.damage(player.getStompDamage()))
					this.playSound("waspStomped");
				break;
			case "top":
				player.knockback(270, posX, KNOCKBACK_STRENGTH, 1);
				player.damage(10);
				break;
			case "left":
			case "right":
				player.knockback(30, posX, KNOCKBACK_STRENGTH, 1);
				player.damage(10);
		}
		return false;
    }  
	
}