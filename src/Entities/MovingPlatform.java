/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author Fredrik
 */
public class MovingPlatform extends Entities.supers.Mobile{
	
	private final int timePerCycle;
	private String moveType = "linear";
	private final int startX;
	private final int startY;
	private int endX;
	private int endY;
	private int waitingTime = 0; // time between platform movements.
	private int toWait = 0;
	private int moveDirX;
	private int moveDirY;
	
	private int curState = 0;
	private final int STATE_TO = 0;
	private final int STATE_WAITING_END = 1;
	private final int STATE_BACK = 2;
	private final int STATE_WAITING_START = 3;
	
	
	/**
	 * Creates a moving platform
	 * @param imgLocation The sprite for the platform.
	 * @param x The platforms initial X coordinate.
	 * @param y The platforms initial Y coordinate.
	 * @param width The width of the platform.
	 * @param height The height of the platform.
	 * @param movement An array of strings in the form of "key", "value". Some keys don't require a value. Ex: new String[]{"right", "200", "up", "50", "linear"}
	 * Full list of keys: up, down, left, right all take the pixel length the platform should move as their value. wait takes milliseconds to wait at each endpoint.
	 * linear, smooth: movement types that don't require a followup value. linear moves at a steady pace. smooth uses acceleration for a more fluid platform.
	 * @param timePerCycle The amount of time it should take the platform to go from A to B in milliseconds.
	 * @throws SlickException 
	 */
	public MovingPlatform(String imgLocation, int x, int y, int width, int height, String[] movement, int timePerCycle) throws SlickException{
		super(createAnim(imgLocation, width, height), Entities.supers.Entity.createModel(x, y, width, height), 9999);
		this.setGravity(0);
		this.startX = x;
		this.startY = y;
		this.endX = x;
		this.endY = y;
		this.timePerCycle = timePerCycle;
		this.parseMovementPattern(movement);
	}
	

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		animations.get("normal").draw(posX-offsetX, posY-offsetY);
	}
	
	@Override
	public void onUpdate(GameContainer gc, int delta){
		switch(moveType){
			case "linear":
				linearMovement(delta);
				break;
			case "smooth":
				smoothMovement(delta);
				break;
		}
		super.onUpdate(gc, delta);
	}
	
	
	/**
	 * The loop for a linearly moving platform.
	 * @param delta Time since last frame.
	 */
	private void linearMovement(int delta){
		switch(curState){
			case STATE_TO:
				if(posX*moveDirX >= endX*moveDirX && posY*moveDirY>= endY*moveDirY){
					curState = STATE_WAITING_END;
					toWait = waitingTime;
					this.setVeloX(0);
					this.setVeloY(0);
					this.setX(endX);
					this.setY(endY);
				} else {
					this.setVeloX((endX - startX) / (timePerCycle/1000));
					this.setVeloY((endY - startY) / (timePerCycle/1000));
				}
				break;
				
			case STATE_WAITING_END:
				toWait -= delta;
				if(toWait <= 0){
					curState = STATE_BACK;
				}
				break;
				
			case STATE_BACK:
				if(posX*moveDirX <= startX*moveDirX && posY*moveDirY <= startY*moveDirY){
					curState = STATE_WAITING_START;
					toWait = waitingTime;
					this.setVeloX(0);
					this.setVeloY(0);
					this.setX(startX);
					this.setY(startY);
				} else {
					this.setVeloX((startX - endX) / (timePerCycle/1000));
					this.setVeloY((startY - endY) / (timePerCycle/1000));
				}
				break;
				
			case STATE_WAITING_START:
				toWait -= delta;
				if(toWait <= 0){
					curState = STATE_TO;
				}
				break;
		}
	}
	
	private void smoothMovement(int delta){
		// TODO: implement this. low priority tho. should be to just accelerate til it reaches the midpoint, then deccelerate at same speed.
		System.out.println("MovingPlatform: smooth movement isn't implemented yet");
	}
	
	/**
	 * Put the info in the String array in to their variables.
	 * @param movement The Array from the constructor.
	 */
	private void parseMovementPattern(String[] movement){
		for(int i=0; i<movement.length; i++){
			switch(movement[i]){
				case "up":
					endY = startY - Integer.parseInt(movement[i+1]);
					i++; // må legge til en ekstra, for neste er bare et tall.
					break;
				
				case "down":
					endY = startY + Integer.parseInt(movement[i+1]);
					i++; // må legge til en ekstra, for neste er bare et tall.
					break;
					
				case "left":
					endX = startX - Integer.parseInt(movement[i+1]);
					i++; // må legge til en ekstra, for neste er bare et tall.
					break;
					
				case "right":
					endX = startX + Integer.parseInt(movement[i+1]);
					i++; // må legge til en ekstra, for neste er bare et tall.
					break;
					
				case "wait":
					waitingTime = Integer.parseInt(movement[i+1]);
					i++; // må legge til en ekstra, for neste er bare et tall.
					break;
					
				case "linear":
					moveType = "linear";
					break;
					
				case "smooth":
					moveType = "smooth";
					break;
				default:
					System.out.println("MovingPlatform: unknown parameter");
			}
		}
		this.moveDirX = endX>startX ? 1 : -1;
		this.moveDirY = endY>startY ? 1 : -1;
	}
	
	private static HashMap<String, Animation> createAnim(String imgLocation, int width, int height) throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		Image img = new Image(imgLocation).getScaledCopy(width, height);
		Animation animNormal =  new Animation(new SpriteSheet(img, width, height), 100);
		animations.put("normal", animNormal);
		return animations;
	}

}
