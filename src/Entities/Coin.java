/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import java.util.HashMap;
import java.util.List;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author storm
 */
public class Coin extends Entities.supers.Item {

	/**
	 * @Param ImageLocation stores the path to the image of the coin-texture
	 * @param width % height limits the size of the coin
	 */
	private static final String FOLDER = "resource/general/coin";				  //location of immage of the coin-sprite
	private static final int WIDTH = 30, HEIGHT = 30;			  //height and width of the coin

	public Coin(int x, int y) throws SlickException {
		//tells the createAnim to make a hashmap and the superclass Entity to make a model.
		super(createAnim(FOLDER+"/RealGold.png", WIDTH, HEIGHT), Entities.supers.Entity.createModel(x, y, WIDTH, HEIGHT));
		this.loadSound("coin", FOLDER+"/pickup.wav");
	}

	@Override
	public void onDeath(List<Entities.supers.Entity> entities) {
		super.onDeath(entities);
		// TODO: play a particle effect and a sound.
	}

	/**
	 * @param imgLocation stored place of the image
	 * @param width	width of the coin
	 * @param height height of the coin
	 * @return a HashMap with animation-states
	 * @throws SlickException
	 */
	private static HashMap<String, Animation> createAnim(String imgLocation, int width, int height) throws SlickException {
		HashMap<String, Animation> animations = new HashMap<>();					      //creates a hashmap of the sprite animations
		Image img = new Image(imgLocation).getScaledCopy(width, height);				      //loads and scales the image from imgLocation
		Animation animNormal = new Animation(new SpriteSheet(img, width, height), 1000);		      //creates a spritesheet for the image by the with and height of the model
		animations.put("normal", animNormal);								      //puts the spritesheet under the normal state (string) so that it can be called by another function to switch states
		return animations;											      //returns the hashmap with the "states" and images
	}

	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {		//takes in  parameters for the movment in the camera as an offset of x and y
		animations.get("normal").draw(posX - offsetX, posY - offsetY);							//draws the immage on the space relative to the given parameteres of the cameramovment
	}
	
	@Override
	public boolean onCollision(Entity e) {
		if(e instanceof Entities.supers.Playable){
			this.playSound("coin");
			return true;
		}
		else return false;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {
	}
}
