/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entities.supers.Entity;
import Entities.supers.Mobile;
import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

/**
 *
 * @author Fredrik
 */
public class Gate extends Entities.supers.Immobile{
	
	private static final int WIDTH = 96;
	private static final int HEIGHT = 256;
	
	private boolean isOpen = false;
	
	public Gate(int x, int y, String folder) throws SlickException{
		super(getAnimations(folder), Entity.createModel(x, y, WIDTH, HEIGHT));
	}
	
	public Gate(int x, int y) throws SlickException{
		super(getAnimations("resource/general/gate"), Entity.createModel(x, y, WIDTH, HEIGHT));
	}
	
	public void open(){
		isOpen = true;
	}
	


	@Override
	public void onRender(GameContainer gc, Graphics g, int offsetX, int offsetY) {
		if(isOpen) 
			animations.get("opening").draw(posX - offsetX, posY - offsetY);
		else 
			animations.get("closed").draw(posX - offsetX, posY - offsetY);	
	}
	
	private static HashMap<String, Animation> getAnimations(String folder) throws SlickException{
		HashMap<String, Animation> animations = new HashMap<>();
		
		Image closed = new Image(folder + "/closed.png").getScaledCopy(WIDTH, HEIGHT);
		Image opening = new Image(folder + "/opening.png").getScaledCopy(WIDTH*22, HEIGHT);
		
		animations.put("closed", new Animation(new SpriteSheet(closed, WIDTH, HEIGHT), 100));
		animations.put("opening", new Animation(new SpriteSheet(opening, WIDTH, HEIGHT), 60));
		
		animations.get("opening").setLooping(false);
		
		return animations;
	}
	
	@Override
	public boolean onCollision(Entity e){
		if(isOpen) return false;
		if(e instanceof Mobile){
			Mobile mob = (Mobile) e;
			String hitPart = this.intersects((Rectangle) e.getBoundingBox(), 3);
			if(hitPart == null) return false;
			switch(hitPart){
				case "bottom":
					if (mob.getVeloY() > 0) mob.setVeloY(0);
					mob.setY(posY - (int)mob.getBoundingBox().getHeight());
					mob.onLanding(0.2);
					break;
					
				case "top":
					if (mob.getVeloY() < 0) mob.setVeloY(0);
					mob.setY(posY + (int)this.getBoundingBox().getHeight());
					break;
					
				case "left":
					if (mob.getVeloX() < 0) mob.setVeloX(0);
					mob.setX(posX + (int)this.getBoundingBox().getWidth());
					break;
					
				case "right":
					if (mob.getVeloX() > 0) mob.setVeloX(0);
					mob.setX(posX - (int)mob.getBoundingBox().getWidth());
					break;
			}
		}
		return false;
	}

	@Override
	public void onUpdate(GameContainer gc, int delta) {
	}
	
}
