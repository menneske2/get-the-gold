/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Henrik og Fredrik
 */
public class MenuMain extends Menu
{
	public static final int sbgID = 0;
	
	int playPosX, playPosY;
	int exitPosX, exitPosY;
	int optionsPosX, optionsPosY;
	Image playImg, playImgActive;
	Image optionsImg, optionsImgActive;
	Image exitImg, exitImgActive;
	Image backgroundImg;
	boolean playActive, optionsActive, exitActive;
	
	public MenuMain() 
    {
		
    }
	
	@Override
	public int getID(){
		return sbgID;
	}
    

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
    {
		super.init(gc, sbg);
		
		// Button names
		playImg = new Image("resource/GUI/buttonPlay.png");
		playImgActive = new Image("resource/GUI/buttonPlayActive.png");
		optionsImg = new Image("resource/GUI/buttonOptions.png");
		optionsImgActive = new Image("resource/GUI/buttonOptionsActive.png");
		exitImg = new Image("resource/GUI/buttonExit.png");
		exitImgActive = new Image("resource/GUI/buttonExitActive.png");
		backgroundImg = new Image("resource/GUI/background.jpg");
		
		// The positions of the different buttons on the main menu. coords starts from top-left corner
		playPosX = (gc.getWidth()-playImg.getWidth())/2;
		playPosY = gc.getHeight()/10*3;
		optionsPosX = (gc.getWidth()-optionsImg.getWidth())/2;
		optionsPosY = gc.getHeight()/10*4;
		exitPosX = (gc.getWidth()-exitImg.getWidth())/2;
		exitPosY = gc.getHeight()/10*5;
		
    }
	
	/**
	 * Fires an event on mouse click.
	 * @param button The value of the button pressed. Use Input.MOUSE_LEFT_BUTTON etc to check.
	 * @param x The X coordinate of the mouse. Starts from the left.
	 * @param y The Y coordinate of the mouse. Starts from the top.
	 * @param clickCount How many times in succession a click was made. I think it's that at least.
	 */
	@Override
	protected void detectMouse(int button, int x, int y){
		if(sbg.getCurrentStateID()==sbgID){
			if(isHoveringOverImage(playImg, playPosX, playPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonPlay(sbg);
				}
			}
			if(isHoveringOverImage(exitImg, exitPosX, exitPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonQuit();
				}
			}

			if(isHoveringOverImage(playImg, optionsPosX, optionsPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonOptions();
				}
			}
		}
			
	}
	
	/**
	 * Fires an event when a key is pressed.
	 * @param key The value of the key pressed. Use Input.KEY_SPACE etc to check.
	 * @param c The char representation of the key pressed. No idea how to use it.
	 */
	@Override
	protected void detectKey(int key, char c){
		if(sbg.getCurrentStateID()==sbgID){
			if(key==Input.KEY_SPACE || key==Input.KEY_ENTER){
				buttonPlay(sbg);
			}
			if(key==Input.KEY_ESCAPE){
				buttonQuit();
			}
			if(key==Input.KEY_O){
				buttonOptions();
			}
		}
	}
	
	@Override
	protected void detectMouseMove(int oldX, int oldY, int newX, int newY) {
		playActive = isHoveringOverImage(playImg, playPosX, playPosY, newX, newY);
		optionsActive = isHoveringOverImage(optionsImg, optionsPosX, optionsPosY, newX, newY);
		exitActive = isHoveringOverImage(exitImg, exitPosX, exitPosY, newX, newY);
	}

    
	@Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
    {
		
		g.setFont(font);
		
		backgroundImg.draw(0,0, gc.getWidth(), gc.getHeight());
		
		if(playActive)	playImgActive.draw(playPosX, playPosY);
		else			playImg.draw(playPosX, playPosY);
		
		if(optionsActive)	optionsImgActive.draw(optionsPosX, optionsPosY);
		else			optionsImg.draw(optionsPosX, optionsPosY);
		
        if(exitActive)	exitImgActive.draw(exitPosX, exitPosY);
		else			exitImg.draw(exitPosX, exitPosY);
		
    }

	
	/**
	 * The "Play" button has been clicked. Switching to overworld view.
	 * @param sbg StateBasedGame object
	 */
	private void buttonPlay(StateBasedGame sbg){
		sbg.enterState(WorldMap.sbgID);
	}
	/**
	 * The "Exit" button has been clicked. Closing the game.
	 */
	private void buttonQuit(){
		System.exit(0);
	}
	/**
	 * The "Options" button has been clicked.
	 */
	private void buttonOptions(){
		sbg.enterState(MenuOptions.sbgID);
	}

	
	
}
