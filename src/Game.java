/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;
import Levels.Level;
import java.awt.Dimension;
import java.awt.Toolkit;
import org.newdawn.slick.state.GameState;
//

/**
 *
 * @author Henrik
 */
public class Game extends StateBasedGame {

    private static final String GAME_NAME = "Get the Gold!";
    private static final int MENU_MAIN = MenuMain.sbgID;
    private static final int MENU_OPTIONS = MenuOptions.sbgID;
    private static final int WORLD_MAP = WorldMap.sbgID;
	private static final int CHARACTER_SELECT = CharacterSelect.sbgID;
	private final ArrayList<Level> levels;
	private final ArrayList<Class> characters;
    
    public Game (String gameName)
    {
        super(gameName);
		levels = new ArrayList<>();
		
		characters = new ArrayList<>();
		registerCharacters();
		
        this.registerState(new MenuMain());
        this.registerState(new MenuOptions());
		this.registerState(new Levels.LevelShop());
		this.registerState(new Levels.Level1());
	    this.registerState(new Levels.Level2());
		this.registerState(new Levels.Level3());
		this.registerState(new Levels.Level4());
        this.registerState(new Levels.Level5());
        this.registerState(new Levels.Level6());
        this.registerState(new Levels.Level7());
        this.registerState(new Levels.Level8());
        this.registerState(new Levels.Level9());
		this.registerState(new Levels.Level10());
		this.registerState(new Levels.Level11());
        
		
		// WorldMap and CharacterSelect must be initialized last, as they need complete lists in their constructor.
		this.registerState(new WorldMap(levels));
		this.registerState(new CharacterSelect(characters));
    }
	
	/**
	 * Register the characters one should be able to pick from at the character select screen.
	 */
	private void registerCharacters(){
		characters.add(Entities.characters.Princess.class);
		characters.add(Entities.characters.Knight.class);
	}

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
		// Initialize the menus.
        this.getState(MENU_MAIN).init(gc, this);
        this.getState(WORLD_MAP).init(gc, this);
        this.getState(MENU_OPTIONS).init(gc, this);
		this.getState(CHARACTER_SELECT).init(gc, this);

        this.enterState(MENU_MAIN);
    }
	
	private void registerState(GameState state){
		this.addState(state);
		
		// Add all levels to the levels array except the shop. this list will be used by worldMap.
		if(state instanceof Level && state.getID() != Levels.LevelShop.sbgID){
			levels.add((Level)state);
		}
	}
	
	
	
	public ArrayList<Level> getLevels(){
		return levels;
	}

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AppGameContainer agc;
        try {
            agc = new AppGameContainer(new Game(GAME_NAME));
			agc.setIcon("resource/general/icon.png");
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			agc.setDisplayMode(screenSize.width, screenSize.height, true);
			agc.setVSync(true);
			agc.setShowFPS(false);
            agc.start();
        } catch (SlickException e) {
        }
    }

}
