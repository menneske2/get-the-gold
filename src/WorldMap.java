/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import Levels.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;



/**
 *
 * @author Fredrik
 */
public class WorldMap extends Menu
{
	public static int sbgID=2;
	private final List<Level> levels;
	
	int page = 0;
	private int selectedLevel;
	private int selectedTile = 0;
	
	
	private String img1Folder, img2Folder, img3Folder, img4Folder;
	
	private Image backgroundImg;
	private Image playImg, backImg, arrowNextImg, arrowPrevImg;
	private Image playImgActive, backImgActive, arrowNextImgActive, arrowPrevImgActive;
	private final String titleText = "Select a level";
	
	private String lvl1text, lvl2text, lvl3text, lvl4text;
	private Image lvl1Img, lvl2Img, lvl3Img, lvl4Img;
	// The positions of the different buttons on the main menu. coords starts from top-left corner
	private int titlePosX, titlePosY;
    private int playPosX, playPosY;
	private int backPosX, backPosY;
	private int arrowPrevX, arrowPrevY, arrowNextX, arrowNextY;
	private int lvl1X, lvl1Y, lvl2X, lvl2Y, lvl3X, lvl3Y, lvl4X, lvl4Y;
	private int lvl1textX, lvl1textY, lvl2textX, lvl2textY, lvl3textX, lvl3textY, lvl4textX, lvl4textY;
	
	private boolean playActive, backActive, arrowPrevActive, arrowNextActive;
	
	// These are mostly used in init(), but are needed for render() to correct people that use the wrong resolution.
	int imgWidth = 300;
	int imgHeight = 150;
    
    public WorldMap(List<Level> levels)
    {
		this.levels = levels;
    }
    

    @Override
    public int getID()
    {
        return sbgID;
    }
	
	/**
	 * Initializes the positions for various GUI elements based on screen size.
	 * @param gc
	 * @param sbg
	 * @throws SlickException 
	 */
    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
    {
        super.init(gc, sbg);
		
		selectedLevel = levels.get(0).getID();
		
		// Update the level images, level names etc.
		updateChoices();
		
		// Constants for the GUI layout.
		int imgSpacingX = 100;
		int imgSpacingY = 100;
		int squaresStartY = 15; // The Y position from which we start drawing the squares. Given in percentage.
		int imgTextSpacing = 15; // The space between a level thumbnail and the level name text.
		
		
		backgroundImg = new Image("resource/GUI/background.jpg");
		arrowNextImg = new Image("resource/GUI/arrowRight.png");
		arrowPrevImg = new Image("resource/GUI/arrowLeft.png");
		arrowNextImgActive = new Image("resource/GUI/arrowRightActive.png");
		arrowPrevImgActive = new Image("resource/GUI/arrowLeftActive.png");
		playImg = new Image("resource/GUI/buttonPlay.png");
		playImgActive = new Image("resource/GUI/buttonPlayActive.png");
		backImg = new Image("resource/GUI/buttonBack.png");
		backImgActive = new Image("resource/GUI/buttonBackActive.png");
		
		// Set the positions for UI elements based on screen size and the constants above.
		titlePosX = (gc.getWidth()-font.getWidth(titleText))/2;
		titlePosY = gc.getHeight()/100*5;
		playPosX = (gc.getWidth()-playImg.getWidth())/2;
		playPosY = gc.getHeight()/10*8;
		backPosX = (gc.getWidth()-backImg.getWidth())/2;
		backPosY = gc.getHeight()/10*9;
		arrowNextX = gc.getWidth()/10 * 8;
		arrowPrevX = gc.getWidth()/10 * 2 - arrowPrevImg.getWidth();
		arrowNextY = gc.getHeight()/100*squaresStartY + imgHeight+imgSpacingY/2 - arrowNextImg.getHeight()/2;
		arrowPrevY = gc.getHeight()/100*squaresStartY + imgHeight+imgSpacingY/2 - arrowPrevImg.getHeight()/2;
		lvl1X = gc.getWidth()/2 - (imgWidth+imgSpacingX/2);
		lvl1Y = gc.getHeight()/100*squaresStartY;
		lvl2X = gc.getWidth()/2 + imgSpacingX/2;
		lvl2Y = gc.getHeight()/100*squaresStartY;
		lvl3X = gc.getWidth()/2 - (imgWidth+imgSpacingX/2);
		lvl3Y = gc.getHeight()/100*squaresStartY + (imgHeight+imgSpacingY);
		lvl4X = gc.getWidth()/2 + imgSpacingX/2;
		lvl4Y = gc.getHeight()/100*squaresStartY + (imgHeight+imgSpacingY);
		
		lvl1textX = gc.getWidth()/2 - (imgSpacingX+imgWidth)/2 - font.getWidth(lvl1text)/2;
		lvl2textX = gc.getWidth()/2 + (imgSpacingX+imgWidth)/2 - font.getWidth(lvl2text)/2;
		lvl3textX = gc.getWidth()/2 - (imgSpacingX+imgWidth)/2 - font.getWidth(lvl3text)/2;
		lvl4textX = gc.getWidth()/2 + (imgSpacingX+imgWidth)/2 - font.getWidth(lvl4text)/2;
		lvl1textY = gc.getHeight()/100*squaresStartY + imgHeight + imgTextSpacing;
		lvl2textY = gc.getHeight()/100*squaresStartY + imgHeight + imgTextSpacing;
		lvl3textY = gc.getHeight()/100*squaresStartY + 2*imgHeight + imgSpacingY + imgTextSpacing;
		lvl4textY = gc.getHeight()/100*squaresStartY + 2*imgHeight + imgSpacingY + imgTextSpacing;
		
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
    {
		backgroundImg.draw(0, 0, gc.getWidth(), gc.getHeight());
		
		// Yellow square around selected level.
		int offset = 15;
		Rectangle selectBox = null;
		switch(selectedTile){
			case 0:
				selectBox = new Rectangle(lvl1X - offset, lvl1Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 1:
				selectBox = new Rectangle(lvl2X - offset, lvl2Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 2:
				selectBox = new Rectangle(lvl3X - offset, lvl3Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
			case 3:
				selectBox = new Rectangle(lvl4X - offset, lvl4Y - offset, imgWidth + 2*offset, imgHeight + 2*offset);
				break;
		}
		
		
		g.setColor(new Color(200, 200, 0, 150)); // r, g, b, a
		g.fill(selectBox);
		
		
		if(playActive)	playImgActive.draw(playPosX, playPosY);
		else			playImg.draw(playPosX, playPosY);
		if(backActive)	backImgActive.draw(backPosX, backPosY);
		else			backImg.draw(backPosX, backPosY);
		if(arrowPrevActive)	arrowPrevImgActive.draw(arrowPrevX, arrowPrevY);
		else				arrowPrevImg.draw(arrowPrevX, arrowPrevY);
		if(arrowNextActive)	arrowNextImgActive.draw(arrowNextX, arrowNextY);
		else				arrowNextImg.draw(arrowNextX, arrowNextY);
		
		g.setColor(Color.white);
        g.setFont(font);
		
		g.drawString(lvl1text, lvl1textX, lvl1textY);
		g.drawString(lvl2text, lvl2textX, lvl2textY);
		g.drawString(lvl3text, lvl3textX, lvl3textY);
		g.drawString(lvl4text, lvl4textX, lvl4textY);
		g.drawString(titleText, titlePosX, titlePosY);
		if(lvl1Img != null) lvl1Img.draw(lvl1X, lvl1Y, imgWidth, imgHeight);
		if(lvl2Img != null) lvl2Img.draw(lvl2X, lvl2Y, imgWidth, imgHeight);
		if(lvl3Img != null) lvl3Img.draw(lvl3X, lvl3Y, imgWidth, imgHeight);
		if(lvl4Img != null) lvl4Img.draw(lvl4X, lvl4Y, imgWidth, imgHeight);
		
    }
	
	@Override
	protected void detectMouseMove(int oldX, int oldY, int newX, int newY) {
		playActive = isHoveringOverImage(playImg, playPosX, playPosY, newX, newY);
		backActive = isHoveringOverImage(backImg, backPosX, backPosY, newX, newY);
		arrowPrevActive = isHoveringOverImage(arrowPrevImg, arrowPrevX, arrowPrevY, newX, newY);
		arrowNextActive = isHoveringOverImage(arrowNextImg, arrowNextX, arrowNextY, newX, newY);
	}
	


	@Override
	protected void detectMouse(int button, int x, int y) {
		if(sbg.getCurrentStateID()==sbgID){
			if(isHoveringOverImage(playImg, playPosX, playPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonPlay();
				}
			}
			if(isHoveringOverImage(backImg, backPosX, backPosY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonBack();
				}
			}
			if(isHoveringOverImage(arrowNextImg, arrowNextX, arrowNextY, x, y)){
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonArrowNext();
				}
			}
			if(isHoveringOverImage(arrowPrevImg, arrowPrevX, arrowPrevY, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					buttonArrowPrev();
				}
			}
			if(isHoveringOverRectangle(lvl1X+imgWidth, lvl1Y+imgHeight, lvl1X, lvl1Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=0;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(lvl2X+imgWidth, lvl2Y+imgHeight, lvl2X, lvl2Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=1;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(lvl3X+imgWidth, lvl3Y+imgHeight, lvl3X, lvl3Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=2;
					select(prevTile);
				}
			}
			if(isHoveringOverRectangle(lvl4X+imgWidth, lvl4Y+imgHeight, lvl4X, lvl4Y, x, y)){ 
				if(button == Input.MOUSE_LEFT_BUTTON){
					int prevTile = selectedTile;
					selectedTile=3;
					select(prevTile);
				}
			}
		}
	}

	@Override
	protected void detectKey(int key, char c) {
		if(sbg.getCurrentStateID()==sbgID){
			if(key==Input.KEY_SPACE || key==Input.KEY_ENTER){
				buttonPlay();
			}
			if(key==Input.KEY_D || key==Input.KEY_RIGHT){
				selectRight();
			}
			if(key==Input.KEY_A || key==Input.KEY_LEFT){
				selectLeft();
			}
			if(key==Input.KEY_W || key==Input.KEY_UP){
				selectUp();
			}
			if(key==Input.KEY_S || key==Input.KEY_DOWN){
				selectDown();
			}
			if(key==Input.KEY_ESCAPE){
				buttonBack();
			}
		}
	}
	
	
	/**
	 * Updates the level previews on screen. Call after changing which pictures should be in the center (which you do by changing the page variable).
	 */
	private void updateChoices(){
		
		try{ img1Folder = levels.get(4*page).getFolder(); } catch(Exception e){ img1Folder = null; }
		try{ img2Folder = levels.get(4*page+1).getFolder(); } catch(Exception e){ img2Folder = null; }
		try{ img3Folder = levels.get(4*page+2).getFolder(); } catch(Exception e){ img3Folder = null; }
		try{ img4Folder = levels.get(4*page+3).getFolder(); } catch(Exception e){ img4Folder = null; }
		
		Path path1;
		Path path2;
		Path path3;
		Path path4;
		
		try { path1 = Paths.get(img1Folder+"/name.txt"); lvl1text = Files.readAllLines(path1).get(0); } catch(Exception e){ lvl1text = ""; }
		try { path2 = Paths.get(img2Folder+"/name.txt"); lvl2text = Files.readAllLines(path2).get(0); } catch(Exception e){ lvl2text = ""; }
		try { path3 = Paths.get(img3Folder+"/name.txt"); lvl3text = Files.readAllLines(path3).get(0); } catch(Exception e){ lvl3text = ""; }
		try { path4 = Paths.get(img4Folder+"/name.txt"); lvl4text = Files.readAllLines(path4).get(0); } catch(Exception e){ lvl4text = ""; }
		
		// Thumbnails are scaled to 300x150 px.
		try{ lvl1Img = new Image(img1Folder+"/thumbnail.png"); } catch(Exception e){ lvl1Img = null; }
		try{ lvl2Img = new Image(img2Folder+"/thumbnail.png"); } catch(Exception e){ lvl2Img = null; }
		try{ lvl3Img = new Image(img3Folder+"/thumbnail.png"); } catch(Exception e){ lvl3Img = null; }
		try{ lvl4Img = new Image(img4Folder+"/thumbnail.png"); } catch(Exception e){ lvl4Img = null; }
			
	}
	
	/**
	 * Takes the player to the shop, and sets the exit of the shop to the chosen level.
	 */
	private void buttonPlay(){
		CharacterSelect charScreen = (CharacterSelect) sbg.getState(CharacterSelect.sbgID);
		charScreen.setNextLevel(selectedLevel);
		sbg.enterState(CharacterSelect.sbgID);
	}
	
	/**
	 * Scrolls the levels on screen to the left.
	 */
	private void buttonArrowNext(){
		page++;
		if(page >= Math.ceil(levels.size() / 4f))
			page=0;
		updateChoices();
		while(!select(selectedTile)){
			selectedTile--;
		}
	}
	
	/**
	 * Scrolls the levels on screen to the right.
	 */
	private void buttonArrowPrev(){
		page--;
		if(page < 0 && Math.floor(levels.size() / 4f) == levels.size() / 4f)
			page = levels.size() / 4 - 1 ;
		else if(page < 0)
			page = (int) Math.floor(levels.size() / 4f);
		updateChoices();
		while(!select(selectedTile)){
			selectedTile--;
		}
	}
	
	/**
	 * Takes you back to the main menu.
	 */
	private void buttonBack(){
		sbg.enterState(MenuMain.sbgID);
	}
	
	/**
	 * Updates the "selectedLevel" variable to the level that is currently highlighted.
	 */
	private boolean select(int prevTile){
		Level level;
		try{ level = levels.get(4*page + selectedTile);
		} catch(Exception e){ level = null; }
		if(level != null){
			selectedLevel = level.getID();
			return true;
		}
		else{
			selectedTile = prevTile;
			return false;
		}
	}
	
	/**
	 * Moves the highlighting reticle one to the right. If it's already at the edge, it calls buttonArrowNext() and sets the highlight to the left.
	 */
	private void selectRight(){
		int prevTile = selectedTile;
		if(selectedTile == 1 || selectedTile == 3){
			selectedTile--;
			buttonArrowNext();
		}
		else{
			selectedTile++;
		}
		select(prevTile);
	}
	
	/**
	 * Moves the highlighting reticle one to the left. If it's already at the edge, it calls buttonArrowPrev() and sets the highlight to the right.
	 */
	private void selectLeft(){
		int prevTile = selectedTile;
		if(selectedTile == 0 || selectedTile == 2){
			selectedTile++;
			buttonArrowPrev();
		}
		else{
			selectedTile--;
		}
		select(prevTile);
	}
	
	/**
	 * Moves the highlighting reticle up. No wraparound.
	 */
	private void selectUp(){
		int prevTile = selectedTile;
		if(selectedTile == 2 || selectedTile == 3){
			selectedTile -= 2;
			select(prevTile);
		}
	}
	
	/**
	 * Moves the highlighting reticle down. No wraparound.
	 */
	private void selectDown(){
		int prevTile = selectedTile;
		if(selectedTile == 0 || selectedTile == 1){
			selectedTile += 2;
			select(prevTile);
		}
	}

	
    
    
}
