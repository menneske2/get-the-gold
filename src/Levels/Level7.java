package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.Coin;
import Entities.Gate;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Henrik
 */
public class Level7 extends Level{
	
	public static int sbgID = 11;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level7";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 47*32; 
                  private Gate gate;
	
	public Level7(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level7.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 36*32, 51*32, 1);
		entities.add(enemy);
                
                LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 28*32, 46*32, 1);
		entities.add(enemy2);
                
                LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 59*32, 52*32, 1);
		entities.add(enemy3);
                
                  CubeLord enemy4 = new CubeLord("CubeLord", 94*32, 32*32, 1);
                                    entities.add(enemy4);
                                    
                                    CubeLord enemy5 = new CubeLord("CubeLord", 126*32, 32*32, 1);
                                    entities.add(enemy5);
                                    
                   CubeLord enemy6 = new CubeLord("CubeLord", 149*32, 32*32, 1);
                                    entities.add(enemy6);
                                    
                   CubeLord enemy7 = new CubeLord("CubeLord", 182*32, 32*32, 1);
                                    entities.add(enemy7);
                
                
                
                 Entities.MovingPlatform movingPlat = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 40*32, 51*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "400", "linear"}, 1000);
		entities.add(movingPlat);
                
                Entities.MovingPlatform movingPlat2 = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 48*32, 48*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "450", "linear"}, 1000);
		entities.add(movingPlat2);
                
                Entities.MovingPlatform movingPlat3 = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 56*32, 45*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "600", "linear"}, 1000);
		entities.add(movingPlat3);
                
                Entities.MovingPlatform movingPlat4 = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 64*32, 42*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "700", "linear"}, 1000);
		entities.add(movingPlat4);
                
                 Entities.MovingPlatform movingPlat5 = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 72*32, 39*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "350", "linear"}, 1000);
		entities.add(movingPlat5);
                
                 Entities.MovingPlatform movingPlat6 = new Entities.MovingPlatform("resource/levels/level7/IceBoxPlatform.png", 80*32, 36*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "650", "linear"}, 1000);
		entities.add(movingPlat6);
                
                
		Entities.Coin coin = new Coin(11*32, 42*32);
		Entities.Coin coin1 = new Coin(19*32, 43*32);
		Entities.Coin coin2 = new Coin(32*32, 44*32);
		Entities.Coin coin3 = new Coin(87*32, 29*32);
		Entities.Coin coin4 = new Coin(98*32, 33*32);
		Entities.Coin coin5 = new Coin(106*32, 37*32);
		Entities.Coin coin6 = new Coin(111*32, 37*32);
		Entities.Coin coin7 = new Coin(117*32, 37*32);
		Entities.Coin coin8 = new Coin(120*32, 37*32);
		Entities.Coin coin9 = new Coin(127*32, 37*32);
		Entities.Coin coin10 = new Coin(139*32, 37*32);
		Entities.Coin coin11 = new Coin(160*32, 42*32);
		Entities.Coin coin12 = new Coin(161*32, 42*32);
		Entities.Coin coin13 = new Coin(162*32, 42*32);
		Entities.Coin coin14 = new Coin(163*32, 42*32);
		Entities.Coin coin15 = new Coin(164*32, 42*32);
		Entities.Coin coin16 = new Coin(158*32, 42*32);
		Entities.Coin coin17 = new Coin(159*32, 42*32);
		Entities.Coin coin18 = new Coin(164*32, 41*32);
		Entities.Coin coin19 = new Coin(165*32, 41*32);
		Entities.Coin coin20 = new Coin(166*32, 41*32);
		Entities.Coin coin21 = new Coin(167*32, 41*32);
		Entities.Coin coin22 = new Coin(168*32, 41*32);
		Entities.Coin coin23 = new Coin(169*32, 41*32);
		Entities.Coin coin24 = new Coin(247*32, 38*32);
		Entities.Coin coin25 = new Coin(253*32, 38*32);
		Entities.Coin coin26 = new Coin(191*32, 38*32);
		Entities.Coin coin27 = new Coin(199*32, 38*32);
		Entities.Coin coin28 = new Coin(208*32, 38*32);
		Entities.Coin coin29 = new Coin(217*32, 38*32);
		Entities.Coin coin30 = new Coin(229*32, 35*32);
		Entities.Coin coin31 = new Coin(241*32, 38*32);

		entities.add(coin);entities.add(coin1);entities.add(coin2);
		entities.add(coin3);entities.add(coin4);entities.add(coin5);
		entities.add(coin6);entities.add(coin7);entities.add(coin8);
		entities.add(coin9);entities.add(coin10);entities.add(coin11);
		entities.add(coin12);entities.add(coin13);entities.add(coin14);
		entities.add(coin15);entities.add(coin16);entities.add(coin17);
		entities.add(coin18);entities.add(coin19);entities.add(coin20);
		entities.add(coin21);entities.add(coin22);entities.add(coin23);
		entities.add(coin24);entities.add(coin25);entities.add(coin26);
		entities.add(coin27);entities.add(coin28);entities.add(coin29);
		entities.add(coin30);entities.add(coin31);
                
                                    Checkpoint checkpoint1 = new Checkpoint(88*32, 36*32);
									Checkpoint checkpoint2 = new Checkpoint(171*32, 39*32);
                                    Checkpoint checkpoint3 = new Checkpoint(226*32, 33*32);
                                    
                                    entities.add(checkpoint1);entities.add(checkpoint2);entities.add(checkpoint3);
                                    
                                    gate = new Gate(334*32, 29*32);
                                    entities.add(gate);
                                   gate.open();
                
	}
        
       
        

    @Override
	public String getFolder() {
		return FOLDER;
	}

}
