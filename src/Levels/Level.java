package Levels;

import Entities.Checkpoint;
import Entities.supers.Entity;
import java.util.List;
import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;
import Entities.supers.Playable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Updates entities, manages the camera, checks some basic things like if the
 * player is in the goal zone and whether or not anything fell off the level.
 *
 * @author Fredrik
 */
public abstract class Level extends BasicGameState {


	protected List<Entity> entities;
	private List<Rectangle> solidBlocks;
	private boolean[][] blockAt;
	protected TiledMap map;
	private int posX = 0;
	private int posY = 0;
	private int spawnX, spawnY;
	
	// The players character.
	protected Class character;
	private Playable player = null;
	
	private int scrWidth; //Screen dimensions. updated to correct valuese in init.
	private int scrHeight;
	private int cameraDistanceX = 700; // updated to scrWidth/2 - playerWidth/2 in init.
	private int cameraDistanceY = 300; // updated to something like the above.
	
	private AudioController music;
	private boolean musicOn = false;
	
	private List<Entity> toBeRemoved; // Can't remove entities mid-update(), so need it here.
	private List<Checkpoint> checkpoints;

	/**
	 * Creates a new Level
	 *
	 * @param spawnX The spawn X coordinate of the player.
	 * @param spawnY The spawn Y coordinate of the player.
	 */
	public Level(int spawnX, int spawnY) {
		this.spawnX = spawnX;
		this.spawnY = spawnY;
	}

	/**
	 * Initializes the level. This includes loading the map and filling it with
	 * entities, setting variables to their correct values etc. Should be called
	 * from worldmap or shop.
	 *
	 * @param gc
	 * @param sbg
	 * @throws SlickException
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		map = this.getMap();
		entities = new ArrayList<>();
		toBeRemoved = new ArrayList<>();
		checkpoints = new ArrayList<>();
		
		scrWidth = gc.getWidth();
		scrHeight = gc.getHeight();
		
		if(character == null) character = Entities.characters.Princess.class;
		try {
			player = (Playable) character.getConstructors()[0].newInstance(spawnX, spawnY-20);
			entities.add(player);
		} catch (Exception ex) {System.out.println("Characters need to have exactly 2 parameters, and they should be spawn positions"); System.exit(420);}
		
		// The player needs an init for input detection.
		player.init(gc, sbg, this.getID());
		
		// Focuses the camera on the player.
		cameraDistanceX = (int) Math.round((scrWidth - player.getBoundingBox().getWidth()) / 2);
		cameraDistanceY = (int) Math.round((scrHeight - player.getBoundingBox().getHeight()) / 2);
		this.posX = (int) player.getPosX() - cameraDistanceX;
		this.posY = (int) player.getPosY() - cameraDistanceY;
		player.setDraw(cameraDistanceX, cameraDistanceY);
		
		solidBlocks = new ArrayList<>();
		fillMapData();
		
		// Initializes the rest of the level.
		this.fillEntities(entities);
		
		for(Entity e : entities){
			e.setLevel(this);
			if(e instanceof Checkpoint) checkpoints.add((Checkpoint)e);
		}
		
		music = AudioController.getAudio();
		music.load("wilhelm", "resource/general/WilhelmScream.wav");
		music.load("applause", "resource/general/applause.wav");
	}

	/**
	 * Lets entities render themselves, and renders the map at the correct
	 * position.
	 *
	 * @param gc
	 * @param sbg
	 * @param g
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		int tileSize = map.getTileWidth();
		
		int xDrawStart = -(posX % tileSize);
		int yDrawStart = -(posY % tileSize);
		int xStart = posX / tileSize;
		int yStart = posY / tileSize;
		
		int playerDrawX = cameraDistanceX;
		int playerDrawY = cameraDistanceY;
		
		// The next 4 ifs are for stopping camerascrolling at the edge of the level.
		if(posX < 0){
			xDrawStart = 0;
			xStart = 0;
			playerDrawX = player.getPosX();
		}
		if(posY < 0){
			yDrawStart = 0;
			yStart = 0;
			playerDrawY = player.getPosY();
		}
		if(posX+gc.getWidth() > map.getWidth()*tileSize){
			int screensFuckedOMeter = gc.getWidth()%tileSize; // Ensures rendering happens correctly even if the screen resolution is not a multiple of tileSize.
			xDrawStart = 0 - screensFuckedOMeter;
			xStart = map.getWidth() - (gc.getWidth() / tileSize)  - (screensFuckedOMeter==0 ? 0 : 1);
			playerDrawX = player.getPosX() - map.getWidth()*tileSize + gc.getWidth();
		}
		if(posY+gc.getHeight() > map.getHeight()*tileSize){
			int screensFuckedOMeter = gc.getHeight()%tileSize;
			yDrawStart = 0 - screensFuckedOMeter;
			yStart = map.getHeight() - (gc.getHeight() / tileSize) - (screensFuckedOMeter==0 ? 0 : 1);
			playerDrawY = player.getPosY() - map.getHeight()*tileSize + gc.getHeight();
		}
		// Make sure the player can't walk off the level.
		if(player.getPosX() <= 0){
			playerDrawX = 0;
			posX = -cameraDistanceX;
		}
		
		player.setDraw(playerDrawX, playerDrawY);
		map.render(xDrawStart, yDrawStart, xStart, yStart, (scrWidth / tileSize) + 2, (scrHeight / tileSize) + 2);
		
		// Calculating offset for entities. this allows them to keep their logical coordinates, but still render in frame.
		int offsetX = posX<0 ? 0 : posX;
		int offsetY = posY<0 ? 0 : posY;
		if(posX+gc.getWidth() > map.getWidth()*tileSize)
			offsetX = map.getWidth()*tileSize - gc.getWidth();
		if(posY+gc.getHeight() > map.getHeight()*tileSize)
			offsetY = map.getHeight()*tileSize - gc.getHeight();
		
		for (Entity e : entities) {
			e.onRender(gc, g, offsetX, offsetY);
		}
	}

	/**
	 * Does everything that needs to be done in a gametick, both for the level
	 * and all entities.
	 *
	 * @param gc
	 * @param sbg
	 * @param delta Time since last frame.
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		for (Entity e : entities) {
			e.onUpdate(gc, delta);
			
			for(Entity e2 : entities) {
				if (e2 != e && e.intersects(e2)) {
					// Collision between entities detected.
					if(e.onCollision(e2)){ // If the entity reported itself to be dead.
						e.onDeath(entities);
						toBeRemoved.add(e);
					} 
				}
			}
			

			if (isInDeathZone(e)) {
				e.onDeath(entities);
				toBeRemoved.add(e);
				if (e instanceof Playable) { // Restart the level if the player died.
					music.play("wilhelm");
					try {
						this.init(gc, sbg);
					} catch (SlickException ex) {}
				}
			}

			// Need access to the fields in Mobile to correct collisions.
			if (e instanceof Entities.supers.Mobile) {
				Entities.supers.Mobile mob = (Entities.supers.Mobile) e;

				boolean airborne = true;
				// Checks for terrain collisions.
				for (Rectangle rect : solidBlocks) {
					if (handleTerrainCollision(mob, rect, delta)) { // returns true if the entity is touching the top of a block.
						airborne = false;
					}
				}
				// Checks for moving platform collisions.
				for (Entity e2 : entities) {
					if (e2 != e && e2 instanceof Entities.MovingPlatform) {
						if (handleTerrainCollision(mob, (Rectangle) e2.getBoundingBox(), delta)) { // returns true if the entity is touching the top of a block.
							airborne = false;
							Entities.MovingPlatform plat = (Entities.MovingPlatform) e2;

							mob.translate(plat.getVeloX() * delta / 1000f, 0);
							if (plat.getVeloY() > 0) {
								mob.translate(0, 1); // Force the player into the platform if he's going down. This prevents him from sliding off.
							}
						}
					}
				}
				if(airborne) mob.onLiftoff();
			}

			if(e == player){
				// Checks if the player has won the level.
				if(crossedFinishLine()){
					try{ 
						onFinish(gc, sbg); 
						break;
					} catch(SlickException ex){}
				}
				// Makes sure the player can't walk off the map at the left side.
				if(player.getPosX() < 0){
					player.setX(0);
					player.setVeloX(0);
				}
				
				// Track the players character with the camera.
				this.posX = player.getPosX()-cameraDistanceX;
				this.posY = player.getPosY()-cameraDistanceY;
			}
		}
		
		if(!musicOn && sbg.getCurrentStateID() == this.getID()){
			music.playMusic(this.getFolder() + "/music.wav");
			musicOn = true;
		}
		
		// clear all dead entities.
		for(Entity e : toBeRemoved){
			entities.remove(e);
			if(e instanceof Playable) try {
				this.init(gc, sbg);
			} catch (SlickException ex) {}
		}
		toBeRemoved.clear();
	}

	/**
	 * Get the block friction for the level. Reccomended value is 0.2.
	 *
	 * @return
	 */
	protected abstract double getBlockFriction();

	/**
	 * Get the "FOLDER" field, which is where the level related files are.
	 * @return 
	 */
	public abstract String getFolder();
	
	/**
	 * Sets the character the player will be using to whatever is put in here.
	 * @param character 
	 */
	public void setCharacter(Class character){
		this.character = character;
	}
	
	/**
	 * Returns the ID for the level. Is never actually used, but hey it's abstract so we have to implement it. We would use it if it was static.
	 * Returns the ID for the level. Is never actually used, but hey it's
	 * abstract so we have to implement it. We would use it if it was static.
	 * @return The sbgID.
	 */
	@Override
	public abstract int getID();

	/**
	 * Should return the tilemap of the level in a .tmx format with gzip
	 * compression.
	 *
	 * @return
	 * @throws SlickException
	 */
	protected abstract TiledMap getMap() throws SlickException;

	/**
	 * Initialize and add entities to the entity list.
	 *
	 * @param entities The list of entities. add shit to it, including the
	 * player.
	 * @throws SlickException
	 */
	protected abstract void fillEntities(List<Entity> entities) throws SlickException;

	/**
	 * Checks if an entity fell below the level.
	 *
	 * @param e The entity you wish to check up on.
	 * @return true if outside, false otherwise.
	 */
	private boolean isInDeathZone(Entity e) {
		int playableHeight = map.getHeight() * map.getTileHeight();
		return e.getPosY() > playableHeight;
	}

	/**
	 * Returns whether or not the player has crossed the finish line, which is the right edge of the map.
	 *
	 * @return true if the finish line is crossed. false otherwise.
	 */
	private boolean crossedFinishLine() {
		int playableWidth = map.getWidth() * map.getTileWidth();
		return player.getPosX() > playableWidth;
	}
	
	public void addEntity(Entity e){
		entities.add(e);
		e.setLevel(this);
	}
	public void removeEntity(Entity e){
		e.onDeath(entities);
		toBeRemoved.add(e);
	}
	
	public void deactivateOtherCheckpoints(Checkpoint check){
		for(Checkpoint c : checkpoints){
			if(c != check) c.setEnabled(false);
		}
	}
	
	/**
	 * Returns whether or not there is a solid block at the given position in the tilemap.
	 * @param x The x coordinate in pixels.
	 * @param y The y coordinate in pixels.
	 * @return True if there's a block there, false otherwise.
	 */
	public boolean isSolidBlockAt(int x, int y){
		int newx = Math.round(x / map.getTileWidth());
		int newy = Math.round(y / map.getTileHeight());
		if(newx < 0 || newy < 0 || newx >= map.getWidth() || newy >= map.getHeight()) return false;
		return blockAt[newx][newy];
	}

	/**
	 * What a level should do when it is finished. The shop is the only one that
	 * shouldn't go straight to worldmap.
	 *
	 * @param gc
	 * @param sbg
	 * @throws SlickException
	 */
	protected void onFinish(GameContainer gc, StateBasedGame sbg) throws SlickException{
		entities.clear();
		music.stopMusic();
		if(sbg.getCurrentStateID() != LevelShop.sbgID)
			music.play("applause");
		gc.getGraphics().destroy();
		sbg.enterState(2); // 2 is the ID for WorldMap. Since it's in the default package, you can't access it with WorldMap.sbgID.
	}
	
	/**
	 * Creates Rectangle objects from the data in the tilemap, then puts the
	 * Rectangles in the solidBlocks list.
	 */
	private void fillMapData() {

		int solidLayer = 1;	// Things you can collide with. The other layers don't have collision.
		int tileSize = map.getTileWidth();
		
		blockAt = new boolean[map.getWidth()][map.getHeight()];
		
		// Initialize everything to false.
		for(boolean[] x : blockAt){
			for(boolean y : x){
				y = false;
			}
		}

		// Tile height and width must be the same. easily fixable if you  want them to be different tho.
		if (tileSize != map.getTileHeight()) {
			System.exit(80085); // The best error code.
		}

		for (int i = 0; i < map.getWidth(); i++) {
			for (int j = 0; j < map.getHeight(); j++) {

				int tileID = map.getTileId(i, j, solidLayer);

				if (tileID != 0) { // filter out air blocks.
					blockAt[i][j] = true;
					solidBlocks.add(new Rectangle((float) i * tileSize, (float) j * tileSize, tileSize, tileSize));
				}
			}
		}
	}

	/**
	 * Makes sure no mobile entity can go through blocks. If the entity is the
	 * player, it also updates the camera position.
	 *
	 * @param mob The Mobile entity that is crashing into something.
	 * @param rect The rectangle it's crashing with.
	 * @param delta Time since last frame.
	 * @return true if it touched the top of the rectangle. false if it was any
	 * other side than top.
	 */
	private boolean handleTerrainCollision(Entities.supers.Mobile mob, Rectangle rect, int delta) {
		String col = mob.intersects(rect, (int) Math.ceil(Math.abs(mob.getVeloY() * delta / 1000f))); //Mathemagics to determine topBox in collision detection.
		boolean standingOnGround = false;
		if (col != null) {
			switch (col) {
				case "top":
					mob.setY((int) (rect.getY() - mob.getBoundingBox().getHeight()));
					if (mob.getVeloY() > 0) mob.setVeloY(0);
					mob.onLanding(this.getBlockFriction());
					standingOnGround = true;
					if (mob instanceof Playable) {
						this.posY = (int) (rect.getY() - mob.getBoundingBox().getHeight() - cameraDistanceY);
					}
					break;

				case "left":
					mob.setX((int) (rect.getX() - mob.getBoundingBox().getWidth()));
					if (mob.getVeloX() > 0) mob.setVeloX(0);
					if (mob instanceof Playable) {
						this.posX = (int) (rect.getX() - mob.getBoundingBox().getWidth() - cameraDistanceX);
					}
					break;

				case "right":
					mob.setX((int) (rect.getX() + rect.getWidth()));
					if (mob.getVeloX() < 0) mob.setVeloX(0);
					if (mob instanceof Playable) {
						this.posX = (int) (rect.getX() + rect.getWidth() - cameraDistanceX);
					}
					break;

				case "bottom":
					mob.setY((int) (rect.getY() + rect.getHeight()));
					if (mob.getVeloY() < 0) mob.setVeloY(0);
					if (mob instanceof Playable) {
						this.posY = (int) (rect.getY() + rect.getHeight() - cameraDistanceY);
					}
					break;
			}
			if(col.equals("left") || col.equals("right")){
				mob.onWallCollision(col);
			}
		}
		return standingOnGround;
	}

	public void setCheckpoint(int x, int y){
		this.spawnX = x;
		this.spawnY = y;
	}
}
