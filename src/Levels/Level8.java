package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Henrik
 */
public class Level8 extends Level{
	
	public static int sbgID = 12;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level8";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 47*32; 
	
	public Level8(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level8.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 106*32, 51*32, 1);
		entities.add(enemy);
                
                LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 30*32, 48*32, 1);
		entities.add(enemy2);
                
                LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 25*32, 48*32, 1);
		entities.add(enemy3);
                
                  CubeLord enemy4 = new CubeLord("CubeLord", 94*32, 32*32, 1);
                                    entities.add(enemy4);
                                    
                                    CubeLord enemy5 = new CubeLord("CubeLord", 126*32, 32*32, 1);
                                    entities.add(enemy5);
                                    
                   CubeLord enemy6 = new CubeLord("CubeLord", 149*32, 32*32, 1);
                                    entities.add(enemy6);
                                    
                   CubeLord enemy7 = new CubeLord("CubeLord", 182*32, 32*32, 1);
                                    entities.add(enemy7);
                
                
                
                 Entities.MovingPlatform movingPlat = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 40*32, 51*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "400", "linear"}, 1000);
		entities.add(movingPlat);
                
                Entities.MovingPlatform movingPlat2 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 48*32, 48*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "450", "linear"}, 1000);
		entities.add(movingPlat2);
                
                Entities.MovingPlatform movingPlat3 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 56*32, 45*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "600", "linear"}, 1000);
		entities.add(movingPlat3);
                
                Entities.MovingPlatform movingPlat4 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 64*32, 42*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "700", "linear"}, 1000);
		entities.add(movingPlat4);
                
                 Entities.MovingPlatform movingPlat5 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 72*32, 39*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "350", "linear"}, 1000);
		entities.add(movingPlat5);
                
                 Entities.MovingPlatform movingPlat6 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 80*32, 36*32, 64, 32, 
				new String[]{"up", "50", "right", "100", "wait", "650", "linear"}, 1000);
		entities.add(movingPlat6);
                
                Entities.MovingPlatform movingPlat7 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 109*32, 54*32, 64, 32, 
				new String[]{"right", "100", "wait", "400", "linear"}, 1000);
		entities.add(movingPlat7);
                
                Entities.MovingPlatform movingPlat8 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 113*32, 50*32, 64, 32, 
				new String[]{"up", "500", "right", "100", "wait", "800", "linear"}, 1000);
		entities.add(movingPlat8);
                
                 Entities.MovingPlatform movingPlat9 = new Entities.MovingPlatform("resource/levels/level8/IceBoxPlatform.png", 162*32, 38*32, 64, 32, 
				new String[]{"right", "100", "wait", "400", "linear"}, 1000);
		entities.add(movingPlat9);
                
                
                                    Checkpoint checkpoint1 = new Checkpoint(103*32, 45*32);
                                    Checkpoint checkpoint2 = new Checkpoint(145*32, 36*32);
                                    Checkpoint checkpoint3 = new Checkpoint(244*32, 36*32);
                                    entities.add(checkpoint1);entities.add(checkpoint2);entities.add(checkpoint3);
                                    
                                    
                
	}

    @Override
	public String getFolder() {
		return FOLDER;
	}


}
