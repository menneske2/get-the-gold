package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Henrik
 */
public class Level9 extends Level{
	
	public static int sbgID = 13;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level9";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 50*32; 
	
	public Level9(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level9.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 2415, 1280, 1);
		entities.add(enemy);
                
                LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 51*32, 52*32, 1);
		entities.add(enemy2);
                
                LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 66*32, 57*32, 1);
		entities.add(enemy3);
                
                LizardBoi enemy4 = new LizardBoi("lizardBoiWater", 69*32, 57*32, 1);
		entities.add(enemy4);
                
                LizardBoi enemy5 = new LizardBoi("lizardBoiWater", 81*32, 54*32, 1);
		entities.add(enemy5);
                
                LizardBoi enemy6 = new LizardBoi("lizardBoiWater", 94*32, 58*32, 1);
		entities.add(enemy6);
                
                LizardBoi enemy7 = new LizardBoi("lizardBoiWater", 96*32, 58*32, 1);
                
		entities.add(enemy7);
                LizardBoi enemy8 = new LizardBoi("lizardBoiWater", 98*32, 58*32, 1);
		entities.add(enemy8);
                
                LizardBoi enemy9 = new LizardBoi("lizardBoiWater", 100*32, 58*32, 1);
		entities.add(enemy9);
                
                LizardBoi enemy10 = new LizardBoi("lizardBoiWater", 102*32, 58*32, 1);
		entities.add(enemy10);
                
                LizardBoi enemy11 = new LizardBoi("lizardBoiWater", 135*32, 56*32, 1);
		entities.add(enemy11);
                
                LizardBoi enemy12 = new LizardBoi("lizardBoiWater", 141*32, 56*32, 1);
		entities.add(enemy12);
                
                LizardBoi enemy13 = new LizardBoi("lizardBoiWater", 155*32, 53*32, 1);
		entities.add(enemy13);
                
                LizardBoi enemy14 = new LizardBoi("lizardBoiWater", 220*32, 52*32, 1);
		entities.add(enemy14);
                
                LizardBoi enemy15 = new LizardBoi("lizardBoiWater", 255*32, 48*32, 1);
		entities.add(enemy15);
                
                LizardBoi enemy16 = new LizardBoi("lizardBoiWater", 282*32, 44*32, 1);
		entities.add(enemy16);
                
                LizardBoi enemy17 = new LizardBoi("lizardBoiWater", 325*32, 37*32, 1);
		entities.add(enemy17);
                
                LizardBoi enemy18 = new LizardBoi("lizardBoiWater", 334*32, 36*32, 1);
		entities.add(enemy18);
                
                LizardBoi enemy19 = new LizardBoi("lizardBoiWater", 398*32, 33*32, 1);
		entities.add(enemy19);
                
                 LizardBoi enemy20 = new LizardBoi("lizardBoiWater", 511*32, 55*32, 1);
		entities.add(enemy20);
                
                LizardBoi enemy21 = new LizardBoi("lizardBoiWater", 519*32, 55*32, 1);
		entities.add(enemy21);
                
                LizardBoi enemy22 = new LizardBoi("lizardBoiWater", 579*32, 42*32, 1);
		entities.add(enemy22);
                  LizardBoi enemy23 = new LizardBoi("lizardBoiWater", 636*32, 40*32, 1);
		entities.add(enemy23);
                
                
              CubeLord cubeLord= new CubeLord("CubeLord", 297*32, 41*32, 1);
                                    entities.add(cubeLord);
               
                                       CubeLord cubeLord2= new CubeLord("CubeLord", 458*32, 43*32, 1);
                                    entities.add(cubeLord2);
                                    
                                   Checkpoint checkpoint1 = new Checkpoint(171*32, 54*32);
                                    Checkpoint checkpoint2 = new Checkpoint(334*32, 36*32);
                                    Checkpoint checkpoint3 = new Checkpoint(553*32, 48*32);
                                    entities.add(checkpoint1);entities.add(checkpoint2);entities.add(checkpoint3);
                
	}

    @Override
	public String getFolder() {
		return FOLDER;
	}


}
