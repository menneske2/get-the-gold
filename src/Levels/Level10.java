package Levels;

import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Storm Ostberg
 */
public class Level10 extends Level {

	public static int sbgID = 16;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level10";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 11 * 32;

	public Level10() {
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}

	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER + "/level.tmx", FOLDER);
	}

	@Override
	public String getFolder() {
		return FOLDER;
	}
	
	Gate gate1;
	Lever lever1;
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException {

		// The platforming section after jumping tutorial.
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 135 * 32, 58 * 32, 96, 32,
			new String[]{"left", "192", "wait", "500", "linear"}, 2000));
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 215 * 32, 92 * 32, 96, 32,
				new String[]{"up", "320", "wait", "500", "linear"}, 2000));
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 190 * 32, 83 * 32, 96, 32,
				new String[]{"up", "320", "wait", "500", "linear"}, 2000));
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 225 * 32, 64 * 32, 96, 32,
				new String[]{"up", "320", "wait", "500", "linear"}, 2000));
		// The enemies by the door at the end.
		LizardBoi LizardBoi = new LizardBoi(16 * 32, 11 * 32, 1);
		entities.add(LizardBoi);
		LizardBoi = new LizardBoi(78 * 32, 67 * 32, 1);
		entities.add(LizardBoi);
		LizardBoi = new LizardBoi(78 * 32, 67 * 32, 1);
		entities.add(LizardBoi);
		// The door at the end.
		gate1 = new Gate(map.getWidth() * map.getTileWidth() - 96, 53 * 32);
		entities.add(gate1);
		lever1 = new Lever(187, 48);
		entities.add(lever1);

		// Coins.
		entities.add(new Coin(64 * 32, 14 * 32));
		entities.add(new Coin(118 * 32, 19 * 32));
		entities.add(new Coin(153 * 32, 43 * 32));
		entities.add(new Coin(96 * 32, 40 * 32));
		entities.add(new Coin(189 * 32, 24 * 32));
		entities.add(new Coin(190 * 32, 24 * 32));
		entities.add(new Coin(191 * 32, 24 * 32));
		entities.add(new Coin(192 * 32, 24 * 32));
		entities.add(new Coin(193 * 32, 24 * 32));
		entities.add(new Coin(194 * 32, 24 * 32));
		entities.add(new Coin(195 * 32, 24 * 32));
		entities.add(new Coin(196 * 32, 24 * 32));
		entities.add(new Coin(197 * 32, 24 * 32));
		entities.add(new Coin(157 * 32, 53 * 32));
		entities.add(new Coin(120 * 32, 60 * 32));
		
		//spikes
		entities.add(new Spike(29, 75));
		for(int i=118; i<=134; i++) entities.add(new Spike(i, 74));
		for(int i=157; i<=161; i++) entities.add(new Spike(i, 64));

		//BouncyPads
		entities.add(new Spring(201, 72, 700));
		entities.add(new Spring(202, 72, 700));
		entities.add(new Spring(193, 59, 700));
		entities.add(new Spring(194, 59, 700));
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		super.update(gc, sbg, delta);
		if(lever1.isOn()) gate1.open();
	}

}
