package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.Coin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rene
 */
public class Level6 extends Level{
	
	public static int sbgID = 10;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level6";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 40*32;  
	
	public Level6(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level6.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 24*32, 41*32, 1);
                LizardBoi enemy1 = new LizardBoi("lizardBoiWater", 135*32, 52*32, 1);
                LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 179*32, 37*32, 1);
                LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 285*32, 41*32, 1);
                LizardBoi enemy4 = new LizardBoi("lizardBoiWater", 340*32, 56*32, 1);
                LizardBoi enemy5 = new LizardBoi("lizardBoiWater", 410*32, 41*32, 1);
		entities.add(enemy);entities.add(enemy1);entities.add(enemy2);
                entities.add(enemy3);entities.add(enemy4);entities.add(enemy5);
                
                Coin coin = new Coin(52*32, 44*32);
                Coin coin1 = new Coin(50*32, 56*32);
                Coin coin2 = new Coin(75*32, 29*32);
                Coin coin3 = new Coin(114*32, 32*32);
                Coin coin4 = new Coin(138*32, 52*32);
                Coin coin5 = new Coin(185*32, 38*32);
                Coin coin6 = new Coin(263*32, 18*32);
                Coin coin7 = new Coin(294*32, 39*32);
                Coin coin8 = new Coin(355*32, 47*32);
                Coin coin9 = new Coin(351*32, 28*32);
                Coin coin10 = new Coin(398*32, 39*32);
                Coin coin11 = new Coin(479*32, 34*32);
                
                entities.add(coin);entities.add(coin1);entities.add(coin2);
                entities.add(coin3);entities.add(coin4);entities.add(coin5);
                entities.add(coin6);entities.add(coin7);entities.add(coin8);
                entities.add(coin9);entities.add(coin10);entities.add(coin11);
                
                Checkpoint check = new Checkpoint(239*32, 43*32);
                entities.add(check);
	}

    @Override
	public String getFolder() {
		return FOLDER;
	}

}
