package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.Coin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rene
 */
public class Level4 extends Level{
	
	public static int sbgID = 8;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level4";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 38*32;  
	
	public Level4(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level4.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 47*32, 40*32, 1);
		LizardBoi enemy1 = new LizardBoi("lizardBoiWater", 73*32, 40*32, 1);
		LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 98*32, 40*32, 1);
		LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 116*32, 39*32, 1);
		LizardBoi enemy4 = new LizardBoi("lizardBoiWater", 173*32, 40*32, 1);
		LizardBoi enemy5 = new LizardBoi("lizardBoiWater", 466*32, 69*32, 1);
		LizardBoi enemy6 = new LizardBoi("lizardBoiWater", 498*32, 69*32, 1);
		LizardBoi enemy7 = new LizardBoi("lizardBoiWater", 637*32, 41*32, 1);
		LizardBoi enemy8 = new LizardBoi("lizardBoiWater", 655*32, 41*32, 1);
		LizardBoi enemy9 = new LizardBoi("lizardBoiWater", 711*32, 41*32, 1);
		LizardBoi enemy10 = new LizardBoi("lizardBoiWater", 800*32, 41*32, 1);
		LizardBoi enemy11 = new LizardBoi("lizardBoiWater", 1355*32, 55*32, 1);
                
		entities.add(enemy);entities.add(enemy1);entities.add(enemy2);
		entities.add(enemy3);entities.add(enemy4);entities.add(enemy5);
		entities.add(enemy6);entities.add(enemy7);entities.add(enemy8);
		entities.add(enemy9);entities.add(enemy10);entities.add(enemy11);

		Coin coin = new Coin(38*32, 39*32);
		Coin coin1 = new Coin(141*32, 40*32);
		Coin coin2 = new Coin(230*32, 49*32);
		Coin coin3 = new Coin(22*32, 59*32);
		Coin coin4 = new Coin(272*32, 30*32);
		Coin coin5 = new Coin(328*32, 42*32);
		Coin coin6 = new Coin(338*32, 45*32);
		Coin coin7 = new Coin(358*32, 50*32);
		Coin coin8 = new Coin(378*32, 54*32);
		Coin coin9 = new Coin(399*32, 58*32);
		Coin coin10 = new Coin(432*32, 64*32);
		Coin coin11 = new Coin(561*32, 42*32);
		Coin coin12 = new Coin(615*32, 42*32);
		Coin coin13 = new Coin(688*32, 38*32);
		Coin coin14 = new Coin(494*32, 25*32);
		Coin coin15 = new Coin(769*32, 50*32);
		Coin coin16 = new Coin(810*32, 42*32);
		Coin coin17 = new Coin(821*32, 31*32);
		Coin coin18 = new Coin(857*32, 27*32);
		Coin coin19 = new Coin(925*32, 20*32);
		Coin coin20 = new Coin(975*32, 19*32);
		Coin coin21 = new Coin(1034*32, 26*32);
		Coin coin22 = new Coin(1098*32, 15*32);
		Coin coin23 = new Coin(1154*32, 18*32);
		Coin coin24 = new Coin(1193*32, 16*32);
		Coin coin25 = new Coin(1236*32, 22*32);
		Coin coin26 = new Coin(1301*32, 42*32);
		Coin coin27 = new Coin(1332*32, 49*32);
		Coin coin28 = new Coin(1363*32, 54*32);
		Coin coin29 = new Coin(1417*32, 65*32);
		Coin coin30 = new Coin(1441*32, 66*32);
		Coin coin31 = new Coin(1463*32, 66*32);

		entities.add(coin);entities.add(coin1);entities.add(coin2);
		entities.add(coin3);entities.add(coin4);entities.add(coin5);
		entities.add(coin6);entities.add(coin7);entities.add(coin8);
		entities.add(coin9);entities.add(coin10);entities.add(coin11);
		entities.add(coin12);entities.add(coin13);entities.add(coin14);
		entities.add(coin15);entities.add(coin16);entities.add(coin17);
		entities.add(coin18);entities.add(coin19);entities.add(coin20);
		entities.add(coin21);entities.add(coin22);entities.add(coin23);
		entities.add(coin24);entities.add(coin25);entities.add(coin26);
		entities.add(coin27);entities.add(coin28);entities.add(coin29);
		entities.add(coin30);entities.add(coin31);
                
                Checkpoint check = new Checkpoint(269*32, 40*32);
                Checkpoint check1 = new Checkpoint(558*32, 41*32);
                Checkpoint check2 = new Checkpoint(1032*32, 30*32);
                
                entities.add(check);entities.add(check1);entities.add(check2);
                
                
	}

    @Override
	public String getFolder() {
		return FOLDER;
	}

}
