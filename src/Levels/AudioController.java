package Levels;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class AudioController{
	
	private static final float VOLUME = -20f; // 0 er default volum. +- decibels.
	private static AudioController snippinator = null;
	private Clip music;
	private final Map<String, Clip> clips;
	
	private AudioController()
	{
		clips = new HashMap<>();
	}
	
	public static AudioController getAudio(){
		if(snippinator == null) snippinator = new  AudioController();
		return snippinator;
	}
	
	/**
	 * Loads up an audio file, so you dont have to lag down the game to load it every time its used.
	 * @param key The name you want to use to activate the clip using(play)
	 * @param path The path + filename of the audio file.
	 */
	public void load(String key, String path){
		if(clips.get(key) != null) return;
		try{
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(path));
			Clip clip = AudioSystem.getClip();
			clip.open(inputStream);
			FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
			volume.setValue(VOLUME);
			clips.put(key, clip);
		} catch(Exception e){
			System.out.println(path + " not found.");
		}
	}
	
	/**
	 * Play a loaded audio file.
	 * @param key the key you registered the audiofile with. 
	 */
	public void play(String key)
	{
		Clip clip = clips.get(key);
		if(clip == null){
			System.out.println("no clip with key " + key + " found");
			return;
		}
		clip.setFramePosition(0);
		clip.start();
	}
	
	/**
	 * Play background music. This can be cancelled with stopMusic(), but you can only have one track going at the same time.
	 * @param path 
	 */
	public void playMusic(String path){
		try{
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(path));
			music = AudioSystem.getClip();
			music.open(inputStream);
			music.loop(Clip.LOOP_CONTINUOUSLY);
			FloatControl volume = (FloatControl) music.getControl(FloatControl.Type.MASTER_GAIN);
			volume.setValue(VOLUME);
		} catch(Exception e){
			System.out.println(path + " not found.");
		}
	}

	public void stopMusic()
	{
		if(music != null) music.close();
	}
        
}