package Levels;


import Entities.enemies.LizardBoi;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public class LevelShop extends Level{
	
	public static int sbgID = 4;
	private static final double SURFACE_FRICTION = 0.2;
	public static final String FOLDER = "resource/levels/shop";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 3500;//480;
	private static final int SPAWN_Y = 1088; //1152 - 64 er bakkenivå.
	private int nextLevel; // The sbgID of the next level.
	
	public LevelShop(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}

	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}
	
	public void setNextLevel(int levelID){
		nextLevel = levelID;
	}
	
	@Override
	protected void onFinish(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Levels.Level level = (Levels.Level) sbg.getState(nextLevel);
		level.setCharacter(character);
		level.init(gc, sbg);
		super.onFinish(gc, sbg);
		sbg.enterState(nextLevel);
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		
	}

	@Override
	public String getFolder() {
		return FOLDER;
	}

}
