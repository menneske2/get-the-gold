package Levels;


import Entities.Checkpoint;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.Coin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rene
 */
public class Level5 extends Level{
	
	public static int sbgID = 9;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level5";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 40*32; 
	
	public Level5(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level5.tmx", FOLDER);
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		LizardBoi enemy = new LizardBoi("lizardBoiWater", 21*32, 29*32, 1);
                LizardBoi enemy1 = new LizardBoi("lizardBoiWater", 102*32, 42*32, 1);
                LizardBoi enemy2 = new LizardBoi("lizardBoiWater", 108*32, 42*32, 1);
                LizardBoi enemy3 = new LizardBoi("lizardBoiWater", 187*32, 40*32, 1);
                LizardBoi enemy4 = new LizardBoi("lizardBoiWater", 197*32, 25*32, 1);
                LizardBoi enemy5 = new LizardBoi("lizardBoiWater", 207*32, 40*32, 1);
                LizardBoi enemy6 = new LizardBoi("lizardBoiWater", 227*32, 40*32, 1);
                LizardBoi enemy7 = new LizardBoi("lizardBoiWater", 244*32, 41*32, 1);
                LizardBoi enemy8 = new LizardBoi("lizardBoiWater", 441*32, 22*32, 1);
                LizardBoi enemy9 = new LizardBoi("lizardBoiWater", 451*32, 42*32, 1);
                LizardBoi enemy10 = new LizardBoi("lizardBoiWater", 472*32, 42*32, 1);
		entities.add(enemy);entities.add(enemy1);entities.add(enemy2);
                entities.add(enemy3);entities.add(enemy4);entities.add(enemy5);
                entities.add(enemy6);entities.add(enemy7);entities.add(enemy8);
                entities.add(enemy9);entities.add(enemy10);
                
                Coin coin = new Coin(25*32, 59*32);
                Coin coin1 = new Coin(24*32, 59*32);
                Coin coin2 = new Coin(23*32, 59*32);
                Coin coin3 = new Coin(22*32, 59*32);
                Coin coin4 = new Coin(21*32, 59*32);
                Coin coin5 = new Coin(20*32, 59*32);
                Coin coin6 = new Coin(19*32, 59*32);
                Coin coin7 = new Coin(47*32, 28*32);
                Coin coin8 = new Coin(97*32, 37*32);
                Coin coin9 = new Coin(134*32, 22*32);
                Coin coin10 = new Coin(211*32, 23*32);
                Coin coin11 = new Coin(316*32, 24*32);
                Coin coin12 = new Coin(380*32, 20*32);
                Coin coin13 = new Coin(432*32, 23*32);
                Coin coin14 = new Coin(494*32, 25*32);
                Coin coin15 = new Coin(259*32, 40*32);
                Coin coin16 = new Coin(265*32, 40*32);
                
                entities.add(coin);entities.add(coin1);entities.add(coin2);
                entities.add(coin3);entities.add(coin4);entities.add(coin5);
                entities.add(coin6);entities.add(coin7);entities.add(coin8);
                entities.add(coin9);entities.add(coin10);entities.add(coin11);
                entities.add(coin12);entities.add(coin13);entities.add(coin14);
                entities.add(coin15);entities.add(coin16);
                
                Checkpoint check = new Checkpoint(186*32, 22*32);
                entities.add(check);
	}

    @Override
	public String getFolder() {
		return FOLDER;
	}

}
