
package Levels;


import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.*;
import Entities.supers.Damageable;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public class Level1 extends Level{
	
	public static int sbgID = 5;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level1";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 49*32;
	
	public Level1(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level.tmx", FOLDER);
	}
	
	@Override
	public String getFolder() {
		return FOLDER;                
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		
		// The platforming section after jumping tutorial.
		entities.add(new Entities.MovingPlatform(FOLDER+"/movingPlatform.png", 130*32, 44*32, 96, 32, 
				new String[]{"left", "192", "wait", "500", "linear"}, 2000));
		entities.add(new Entities.MovingPlatform(FOLDER+"/movingPlatform.png", 136*32, 44*32, 96, 32, 
				new String[]{"up", "320", "wait", "500", "linear"}, 2000));

		// The enemies by the door at the end.
        TutorialEnemy tutEnemy = new TutorialEnemy(162*32, 31*32, 1);
		entities.add(tutEnemy);
		tutEnemy = new TutorialEnemy(191*32, 31*32, 1);
		tutEnemy.stompImmune = true;
		entities.add(tutEnemy);
		
		// The door at the end.
		Gate gate = new Gate(map.getWidth()*map.getTileWidth()-96, 26*32);
		entities.add(gate);
		
		// Coins.
		entities.add(new Coin(46*32, 48*32));
		entities.add(new Coin(49*32, 48*32));
		entities.add(new Coin(83*32, 42*32));
		entities.add(new Coin(96*32, 43*32));
		entities.add(new Coin(109*32, 42*32));
		entities.add(new Coin(131*32, 42*32));
		entities.add(new Coin(137*32, 32*32));
		entities.add(new Coin(163*32, 30*32));
		entities.add(new Coin(192*32, 30*32));
	}
	
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		super.update(gc, sbg, delta);
		int aliveThings = 0;
		Gate gate = null;
		for(Entity e : entities){
			if(e instanceof Damageable){
				aliveThings++;
			}
			if(e instanceof Gate){
				gate = (Gate)e;
			}
		}
		if(aliveThings == 1){
			gate.open();
		}
	}

}
