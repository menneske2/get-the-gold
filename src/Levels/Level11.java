package Levels;

import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Storm Ostberg
 */
public class Level11 extends Level {

	public static int sbgID = 17;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level11";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 1 * 32;
	private static final int SPAWN_Y = 11 * 32;

	public Level11() {
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}

	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER + "/level.tmx", FOLDER);
	}

	@Override
	public String getFolder() {
		return FOLDER;
	}

	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException {

		// The platforming section after jumping tutorial.
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 230 * 32, 73 * 32, 96, 32,
			new String[]{"up", "320", "wait", "500", "linear"}, 2000));
		entities.add(new Entities.MovingPlatform(FOLDER + "/movingPlatform.png", 213 * 32, 51 * 32, 96, 32,
			new String[]{"left", "192", "wait", "500", "linear"}, 2000));

		// The enemies by the door at the end.
		TutorialEnemy tutEnemy = new TutorialEnemy(162 * 32, 31 * 32, 1);
		entities.add(tutEnemy);
		tutEnemy = new TutorialEnemy(231 * 32, 49 * 32, 1);
		tutEnemy.stompImmune = true;
		entities.add(tutEnemy);


		// Coins.
		entities.add(new Coin(6*32, 12*32));
		for(int i=61; i<=65; i++) entities.add(new Coin(i*32, 63*32));
		for(int i=141; i<=76; i++) entities.add(new Coin(i*32, 76*32));
		for(int i=190; i<=194; i++) entities.add(new Coin(i*32, 68*32));
		

		// Spikes
		for (int i = 66; i <= 96; i++) {
			entities.add(new Spike(i, 69));
		}
		for (int i = 135; i <= 235; i++) {
			entities.add(new Spike(i, 92));
		}
		entities.add(new Spring(115, 88, 700));
		entities.add(new Spring(116, 88, 700));
	}


}
