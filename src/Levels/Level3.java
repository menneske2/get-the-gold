
package Levels;


import Entities.Spike;
import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.*;
import Entities.supers.Damageable;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public class Level3 extends Level{
	
	public static int sbgID = 7;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level3";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 46*32;
	
	public Level3(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level.tmx", FOLDER);
	}
	
	@Override
	public String getFolder() {
		return FOLDER;                
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		super.update(gc, sbg, delta);
		if(lever1.isOn()) gate1.open();
		boolean allDead = true;
		for(Damageable e : entities2){
			if(e.getHP() > 0) allDead = false;
		}
		if(allDead) gate2.open();
	}
	
	Gate gate1;
	Lever lever1;
	Gate gate2;
	Damageable[] entities2;
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		// Checkpoints ---------------------------------------
		entities.add(new Checkpoint(90*32-16, 74*32));
		entities.add(new Checkpoint(218*32, 23*32));
		
		//Enemies -------------------------------------------------------------------
		entities.add(new LizardBoi("lizardBoiWater", 195*32, 54*32, 1));
		entities.add(new Wasp(135*32 + 8, 24*32, 1)); // 47 bred. 64-47 = 17. 8 på hver side.
		entities.add(new Wasp(129*32 + 8, 24*32, 1));
		entities2 = new Damageable[8];
		entities2[0] = new LizardBoi("lizardBoiWater", 280*32, 75*32, 1);
		entities2[1] = new LizardBoi("lizardBoiWater", 290*32, 75*32, 1);
		entities2[2] = new LizardBoi("lizardBoiWater", 300*32, 75*32, 1);
		entities2[3] = new LizardBoi("lizardBoiWater", 310*32, 75*32, 1);
		entities2[4] = new LizardBoi("lizardBoiWater", 320*32, 75*32, 1);
		entities2[5] = new Wasp(284*32 + 8, 71*32, 1);
		entities2[6] = new Wasp(295*32 + 8, 71*32, 1);
		entities2[7] = new Wasp(314*32 + 8, 71*32, 1);
		for(Entity e : entities2) entities.add(e);
		
		//Other entities ------------------------------------------------------------
			// entrance
		for(int i=50; i<=61; i++) entities.add(new Spike(i, 54));
		for(int i=63; i<=73; i++) entities.add(new Spike(i, 54));
			// tunnels.
		entities.add(new Spike(29, 75));
		for(int i=21; i<=35; i++) entities.add(new Spike(i, 78));
		for(int i=37; i<=61; i++) entities.add(new Spike(i, 78));
		for(int i=63; i<=72; i++) entities.add(new Spike(i, 78));
		for(int i=74; i<=86; i++) entities.add(new Spike(i, 78));
		for(int i=94; i<=106; i++) entities.add(new Spike(i, 78));
		for(int i=108; i<=117; i++) entities.add(new Spike(i, 78));
		for(int i=119; i<=131; i++) entities.add(new Spike(i, 78));
		for(int i=135; i<=161; i++) entities.add(new Spike(i, 78));
		for(int i=164; i<=169; i++) entities.add(new Spike(i, 78));
		for(int i=172; i<=199; i++) entities.add(new Spike(i, 78));
			// the place where you go up.
		entities.add(new Spring(212, 51, 700));
		entities.add(new Spring(213, 51, 700));
		entities.add(new MovingPlatform(FOLDER+"/movingPlatform.png", 201*32, 44*32, 96, 32, 
				new String[]{"right", "384", "wait", "0", "linear"}, 2000));
		entities.add(new MovingPlatform(FOLDER+"/movingPlatform.png", 213*32, 40*32, 96, 32, 
				new String[]{"left", "384", "wait", "0", "linear"}, 2000));
		entities.add(new MovingPlatform(FOLDER+"/movingPlatform.png", 201*32, 36*32, 96, 32, 
				new String[]{"right", "384", "wait", "0", "linear"}, 2000));
		entities.add(new MovingPlatform(FOLDER+"/movingPlatform.png", 213*32, 32*32, 96, 32, 
				new String[]{"left", "384", "wait", "0", "linear"}, 2000));
		entities.add(new MovingPlatform(FOLDER+"/movingPlatform.png", 201*32, 28*32, 96, 32, 
				new String[]{"right", "384", "wait", "0", "linear"}, 2000));
			// left of place where you go up.
		lever1 = new Lever(120, 32);
		entities.add(lever1);
		for(int i=125; i<=128; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=131; i<=134; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=137; i<=140; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=152; i<=155; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=158; i<=161; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=164; i<=167; i++) entities.add(new Spring(i, 46, 1150));
		for(int i=129; i<=130; i++) entities.add(new Spike(i, 27));
		for(int i=135; i<=136; i++) entities.add(new Spike(i, 27));
		for(int i=156; i<=157; i++) entities.add(new Spike(i, 27));
		for(int i=162; i<=163; i++) entities.add(new Spike(i, 27));
		for(int i=175; i<=177; i++) entities.add(new Spring(i, 32, 700));
			// right of place where you go up.
		gate1 = new Gate(253*32, 21*32);
		entities.add(gate1);
			// final area
		gate2 = new Gate(344*32, 70*32);
		entities.add(gate2);
		
		
		//Coins ---------------------------------------------------------------------
		entities.add(new Coin(62*32, 49*32));
		for(int i=15; i<=20; i++) entities.add(new Coin(i*32, 76*32));
		for(int i=16; i<=19; i++) entities.add(new Coin(i*32, 75*32));
		for(int i=17; i<=18; i++) entities.add(new Coin(i*32, 74*32));
		entities.add(new Coin(28*32, 76*32));
		entities.add(new Coin(41*32, 72*32));
		entities.add(new Coin(36*32, 76*32));
		entities.add(new Coin(130*32, 72*32));
		entities.add(new Coin(136*32, 72*32));
		for(int i=170; i<=171; i++) entities.add(new Coin(i*32, 78*32));
		for(int i=148; i<=149; i++) entities.add(new Coin(i*32, 69*32));
		for(int i=174; i<=184; i+=5) entities.add(new Coin(i*32, 74*32));
		for(int i=185; i<=195; i+=5) entities.add(new Coin(i*32, 74*32));
		entities.add(new Coin(217*32, 53*32));
		for(int i=32; i<=45; i++) entities.add(new Coin(166*32, i*32));
		for(int i=32; i<=45; i++) entities.add(new Coin(153*32, i*32));
		for(int i=32; i<=45; i++) entities.add(new Coin(139*32, i*32));
		for(int i=32; i<=45; i++) entities.add(new Coin(126*32, i*32));
		for(int i=31; i<=64; i++) entities.add(new Coin((265+(i-31)%5)*32, i*32));
	}

}
