
package Levels;


import Entities.supers.Entity;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import java.util.List;
import Entities.enemies.*;
import Entities.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public class Level2 extends Level{
	
	public static int sbgID = 6;
	private static final double SURFACE_FRICTION = 0.2;
	private final String FOLDER = "resource/levels/level2";
	// The spawn coordinates of the player.
	private static final int SPAWN_X = 0;
	private static final int SPAWN_Y = 68*32;
	
	public Level2(){
		super(SPAWN_X, SPAWN_Y);
	}

	@Override
	public int getID() {
		return sbgID;
	}
	
	@Override
	protected double getBlockFriction() {
		return SURFACE_FRICTION;
	}

	@Override
	protected TiledMap getMap() throws SlickException {
		return new TiledMap(FOLDER+"/level.tmx", FOLDER);
	}
	
	@Override
	public String getFolder() {
		return FOLDER;                
	}
	
	@Override
	protected void fillEntities(List<Entity> entities) throws SlickException{
		// Checkpoints  -------------------------------------------------------------
		entities.add(new Checkpoint(144*32, 40*32));
		entities.add(new Checkpoint(249*32, 25*32));
		
		//Enemies -------------------------------------------------------------------
		entities.add(new LizardBoi(56*32, 64*32, 1));
		entities.add(new LizardBoi(105*32, 64*32, 1));
		entities.add(new LizardBoi(130*32, 40*32, 1));
		entities.add(new LizardBoi(134*32, 40*32, 1));
		entities.add(new LizardBoi(200*32, 74*32, 4).setMaxHP(90));
		entities.add(new LizardBoi(230*32, 74*32, 4).setMaxHP(90));
		entities.add(new LizardBoi(231*32, 49*32, 1));
		entities.add(new LizardBoi(250*32, 52*32, 1));
		entities.add(new LizardBoi(250*32, 52*32, 1));
		entities.add(new LizardBoi("lizardBoiWater", 359*32, 64*32, 1));
		
		//Coins ---------------------------------------------------------------------
		entities.add(new Coin(86*32, 69*32));
		entities.add(new Coin(115*32, 37*32));
		// "Secret" area
		for(int i=140; i<145; i++){
			for(int j=60; j<66; j++) entities.add(new Coin(i*32, j*32));
		}
		entities.add(new Coin(141*32, 59*32));
		entities.add(new Coin(142*32, 59*32));
		entities.add(new Coin(143*32, 59*32));
		// "Secret" 2
		for(int i=49; i<53; i++) entities.add(new Coin(250*32, i*32));
		// monster pit
		for(int i=213; i<235; i+=2) entities.add(new Coin(i*32, 78*32));
		entities.add(new Coin(257*32, 32*32));
		// Jumping pillars
		for(int i=0; i<7; i++){
			entities.add(new Coin((266+12*i)*32, (30+5*i)*32));
			entities.add(new Coin((267+12*i)*32, (30+5*i)*32));
		}
		entities.add(new Coin(355*32, 26*32));
		entities.add(new Coin(355*32, 27*32));
		entities.add(new Coin(355*32, 28*32));
		entities.add(new Coin(349*32, 16*32));
		entities.add(new Coin(350*32, 16*32));
	}
	
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		super.update(gc, sbg, delta);
	}


}
